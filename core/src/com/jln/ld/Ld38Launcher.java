package com.jln.ld;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.jln.ld.menus.GameMenu;
import com.jln.ld.menus.GameSave;
import com.jln.ld.menus.PlayerSelectionMenu;
import com.jln.ld.menus.StageSelectionMenu;
import com.jln.ld.menus.TitleMenu;

public class Ld38Launcher extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
	
	private GameMenu gameMenu;
	private PlayerSelectionMenu playerSelectionMenu;
	private StageSelectionMenu stageSelectionMenu;
	private TitleMenu titleMenu;
	private int gameMode = 0;
	
	public static int BEST_SCORE = 0;
	public static int LAST_SCORE = 0;
	
	/**
	 * Creation des ressources
	 */
	@Override
	public void create () {
		titleMenu = new TitleMenu();
		gameMenu = new GameMenu();
		playerSelectionMenu = new PlayerSelectionMenu();
		stageSelectionMenu = new StageSelectionMenu();
		GameShared.instance.gameBatch = new SpriteBatch();
		GameShared.instance.menuBatch = new SpriteBatch();
		GameShared.instance.shapeRenderer = new ShapeRenderer();
		GameShared.instance.shapeRenderer.setAutoShapeType(true);
		

		// Generation de la font
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("menus/pixelOperator8.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 14;
		parameter.spaceX = 0;
		GameShared.instance.font = generator.generateFont(parameter);
		GameShared.instance.font.setColor(Color.WHITE);
		
		FreeTypeFontParameter parameter2 = new FreeTypeFontParameter();
		parameter2.size = 10;
		parameter2.spaceX = 0;
		GameShared.instance.smallFont = generator.generateFont(parameter2);
		GameShared.instance.smallFont.setColor(Color.WHITE);
		generator.dispose();
		
		// Generation de la sauvegarde
  		GameShared.instance.save = new GameSave();
		GameShared.instance.save.load();
	}

	/**
	 * Boucle principale de jeu
	 */
	@Override
	public void render () {
		
		if(gameMode == 0){
			titleMenu.update(this);
			titleMenu.render(this);
		}else if(gameMode == 1){
			playerSelectionMenu.update(this);
			playerSelectionMenu.render(this);
		}else if(gameMode == 2){
			stageSelectionMenu.update(this);
			stageSelectionMenu.render(this);
		}else if(gameMode == 3){
			gameMenu.update(this);
			gameMenu.render(this);
		}
	}
	
	/**
	 * Bascule vers le mode de jeu principal
	 */
	public void goToGameMode(){
		gameMode = 3;
		gameMenu.init();
		gameMenu.launchMusic();
	}
	
	/**
	 * Bascule vers la selection des joueurs
	 */
	public void goToPlayerSelectionMode(){
		gameMode = 1;
		playerSelectionMenu.init();
	}
	/**
	 * Bascule vers la selection de niveau
	 */
	public void goToStageSelectionMode(){
		gameMode = 2;
		stageSelectionMenu.init();
	}
	
	/**
	 * Bascule vers l'ecran titre
	 */
	public void goToTitleMenu(){
		gameMode = 0;
		gameMenu.stopMusic();
		gameMenu.init();
		playerSelectionMenu.init();
		stageSelectionMenu.init();
	}
}
