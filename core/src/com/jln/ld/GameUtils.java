package com.jln.ld;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public abstract class GameUtils {

	/**
	 * @param textureRegion la grille de sprites
	 * @param nbFrames le nombre de frame
	 * @param line l'index de la ligne
	 * @param col l'index de la colonne
	 * @param frameDuration la dur�e de chaque frame
	 * @return l'objet animation cr��
	 */
	public static Animation loadAnimation(TextureRegion[][] textureRegion, int nbFrames, int line, float frameDuration){
		TextureRegion[] frames = new TextureRegion[nbFrames];
		for (int i = 0; i < nbFrames; i++) {
			frames[i] = textureRegion[line][i];
		}
		return new Animation(frameDuration, frames);
	}
}
