package com.jln.ld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jln.ld.map.MapItem;
import com.jln.ld.menus.GameMenu;

public class GameHud {

	private boolean onPause = false;
	private boolean onGameOver = false;
	private TextureRegion[][] icons;
	
	private int nbKills = 0, cptKills = 0;
	private int nbLife = 3, cptLifes = 0;
	private int nbMonsters = 0, cptMonsters = 0;
	
	/**
	 * Constructeur
	 */
	public GameHud(){
		icons = TextureRegion.split(GameConstant.HUD_ICONS, 24, 24);
	}
	
	/**
	 * Dessine le HUD
	 * @param game
	 */
	public void render(GameMenu game){
		
		GameShared.instance.menuBatch.draw(icons[0][(cptMonsters>0?1:0)],4,332);
		GameShared.instance.menuBatch.draw(icons[1][(cptLifes>0?1:0)],4,310);
		GameShared.instance.menuBatch.draw(icons[2][(cptKills>0?1:0)],580,332);

		GameShared.instance.font.setColor(0.22f, 0.13f, 0.24f, 1f);
		GameShared.instance.font.draw(GameShared.instance.menuBatch, nbMonsters+"",33,351);
		GameShared.instance.font.draw(GameShared.instance.menuBatch, nbLife+"",33,327);
		GameShared.instance.font.draw(GameShared.instance.menuBatch, nbKills+"",603,351);
		
		GameShared.instance.font.setColor(1f, 1f, 1f, 1f);
		GameShared.instance.font.draw(GameShared.instance.menuBatch, nbMonsters+"",32,352);
		GameShared.instance.font.draw(GameShared.instance.menuBatch, nbLife+"",32,328);
		GameShared.instance.font.draw(GameShared.instance.menuBatch, nbKills+"",602,352);
		
		
		int item = game.getHeros().getItem();
		if(item != MapItem.NO_ITEM){
			GameShared.instance.menuBatch.draw(GameConstant.HUD_ITEM_BAR,208,4);
			GameShared.instance.menuBatch.draw(GameConstant.HUD_ITEM_BAR_CONTENT,220,8,0,0,game.getHeros().getItemTime()/2,6);
			if(item == MapItem.WEAPON_SHOVEL){
				GameShared.instance.menuBatch.draw(GameConstant.HUD_ITEM_BAR_NAMES[0][0],288,12);
			}else if(item == MapItem.ITEM_FEATHER){
				GameShared.instance.menuBatch.draw(GameConstant.HUD_ITEM_BAR_NAMES[2][0],288,12);
			}else if(item == MapItem.WEAPON_HAMMER){
				GameShared.instance.menuBatch.draw(GameConstant.HUD_ITEM_BAR_NAMES[1][0],288,12);
			}else if(item == MapItem.ITEM_IRON_POTION){
				GameShared.instance.menuBatch.draw(GameConstant.HUD_ITEM_BAR_NAMES[3][0],288,12);
			}
		}
		
		if(onPause){
			GameShared.instance.menuBatch.draw(GameConstant.HUD_PAUSE,0,0);
		}
		if(onGameOver){
			GameShared.instance.menuBatch.draw(GameConstant.HUD_GAMEOVER,0,0);
		}
	}
	
	/**
	 * Met a jour le HUD
	 * @param game
	 */
	public void update(GameMenu game){
		if(Gdx.input.isKeyJustPressed(Keys.ESCAPE)){
			onPause = !onPause;
		}
		
//		if(Gdx.input.isKeyJustPressed(Keys.Q)){
//			onGameOver = true;
//		}
		
		if(cptMonsters > 0){
			cptMonsters --;
		}
		if(cptKills > 0){
			cptKills --;
		}
		if(cptLifes > 0){
			cptLifes --;
		}
		
		if(GameConstant.DEBUG && Gdx.input.isKeyJustPressed(Keys.D)){
			System.out.println("X="+game.getHeros().getX()+" y="+game.getHeros().getY());
		}
	}
	
	/**
	 * Permet de savoir si le jeu est en pause
	 * @return vrai si le jeu est en pause
	 */
	public boolean isOnPause(){
		return onPause;
	}
	
	/**
	 * Permet de savoir si le jeu est game over
	 * @return vrai si le jeu est game over
	 */
	public boolean isOnGameOver(){
		return onGameOver;
	}
	
	/**
	 * Met a jour le nombre de mort
	 * @param game
	 */
	public void updateMonsters(GameMenu game, boolean flashIcon){
		this.nbMonsters = game.getCharacters().size()-1;
		if(flashIcon){
			this.cptMonsters = 60;
		}
	}
	
	/**
	 * Met a jour le nombre de mort
	 * @param game
	 */
	public void updateKills(GameMenu game, int point, boolean flashIcon){
		this.nbKills+=point;
		if(flashIcon){
			this.cptKills = 60;
		}
	}
	
	/**
	 * Met a jour le nombre de mort
	 * @param game
	 */
	public void updateLife(GameMenu game, boolean flashIcon, int decalLife){
		this.nbLife+=decalLife;
		if(flashIcon){
			this.cptLifes = 60;
		}
	}
	
	/**
	 * Initialise l'indi
	 * @param game
	 */
	public void callNextWave(GameMenu game){
		
	}
	
	public int getLife(){
		return nbLife;
	}
	
	public void gameOver(GameMenu game){
		int score = (game.getWaveNo()*10)+nbKills;
		Ld38Launcher.LAST_SCORE = score; 
		if(score>Ld38Launcher.BEST_SCORE){
			Ld38Launcher.BEST_SCORE = score; 
		}
		GameShared.instance.save.saveArcadeScore(score, game.getMap().getMapCode()-1);
		this.onGameOver = true;
	}
}
