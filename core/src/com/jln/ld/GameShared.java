package com.jln.ld;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.jln.ld.menus.GameSave;
import com.jln.ld.menus.PlayerSelectionItem;
import com.jln.ld.menus.PlayerSelectionMenu;

public class GameShared {
	
	public  static GameShared instance = new GameShared();
	public  SpriteBatch gameBatch;
	public  SpriteBatch menuBatch;
	public  ShapeRenderer  shapeRenderer;
	public BitmapFont font, smallFont;
	public PlayerSelectionItem player1Character;
	public int levelNumber = 1;
	public GameSave save;
	
	private GameShared(){
		// SINGLETON
	}
}
