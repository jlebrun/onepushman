package com.jln.ld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public abstract class GameConstant {
	
	public static final boolean DEBUG = false;
	
	/*
	 * CONSTANTES DE DIRECTION
	 */
	public static final int DIR_DOWN = 0;
	public static final int DIR_LEFT = 1;
	public static final int DIR_RIGHT = 2;
	public static final int DIR_UP = 3;
	public static final int DIR_FALL = 4;
	
	/*
	 * CONSTANTES DE JEU
	 */
	public static final int GAME_WIDTH = 640;
	public static final int GAME_HEIGHT = 360;
	public static final String GAME_TITLE = "One Push Man";
	
	
	/* ***********************************************
	 * TEXTURES
	 */
	
	// TITLE
	public static final Texture TITLE_BACKGROUND = new Texture("menus/title.png");
	public static final Texture TITLE_NEWGAME = new Texture("menus/newGame.png");
	public static final Texture TITLE_QUIT = new Texture("menus/quit.png");
	public static final Texture TITLE_SELECTION = new Texture("menus/title_selection.png");
	public static final TextureRegion[][] TITLE_FRAMES = TextureRegion.split(new Texture("menus/title_cases.png"),128,24);
	
	// Choix des personnages
	public static final TextureRegion[][] SELEC_CHARACTERS_PLAYER_FRAMES = TextureRegion.split(new Texture("menus/chara_selec_characters.png"),96,96);
	public static final TextureRegion[][] SELEC_CHARACTERS_SELEC_FRAMES = TextureRegion.split(new Texture("menus/chara_selec_cases.png"),96,96);
	public static final Texture SELEC_CHARACTERS_BACKGROUND = new Texture("menus/chara_selec_background.png");
	public static final Texture SELEC_CHARACTERS_MENU = new Texture("menus/chara_selec_menu.png");
	public static final Texture SELEC_CHARACTERS_PLAYER_BOX = new Texture("menus/chara_selec_player_box.png");
	public static final TextureRegion[][] SELEC_CHARACTERS_STAR = TextureRegion.split(new Texture("menus/chara_selec_star.png"),16,16);
	public static final Texture SELEC_CHARACTERS_LOCKED_ICON = new Texture("menus/chara_selec_locked_icon.png");
	
	// Choix du stage
	public static final TextureRegion[][] SELEC_STAGE_FRAMES = TextureRegion.split(new Texture("menus/stage_selec_stages.png"),256,180);
	public static final TextureRegion[][] SELEC_STAGE_SELEC_FRAMES = TextureRegion.split(new Texture("menus/stage_selec_cases.png"),256,180);
	public static final Texture SELEC_STAGE_BACKGROUND = new Texture("menus/stage_selec_background.png");
	public static final Texture SELEC_STAGE_MENU = new Texture("menus/stage_selec_menu.png");
	public static final Texture SELEC_STAGE_BEST_SCORE_OVER = new Texture("menus/stage_selec_best_score_over.png");
	
	// Boutons de validation
	public static final Texture SELEC_VALID = new Texture("menus/chara_selec_valid.png");
	public static final Texture SELEC_VALID_SELEC = new Texture("menus/chara_selec_valid_selec.png");
	
	// HUD
	public static final Texture DEBUG_POINT = new Texture("menus/point.png");
	public static final Texture HUD_PAUSE = new Texture("menus/pause.png");
	public static final Texture HUD_GAMEOVER = new Texture("menus/gameover.png");
	public static final Texture HUD_ICONS = new Texture("menus/icons.png");
	
	public static final Texture HUD_ITEM_BAR = new Texture("menus/item_bar.png");
	public static final Texture HUD_ITEM_BAR_CONTENT = new Texture("menus/item_bar_bar.png");
	public static final TextureRegion[][] HUD_ITEM_BAR_NAMES= TextureRegion.split(new Texture("menus/item_bar_names.png"),64,16);
	
	
	
	// CHARACTERS
	public static final Texture TX_APPEAR = new Texture("map_animations/appear.png"); // 64*228
	
	public static final Texture TX_GREENBOY = new Texture("characters/greenboy.png");
	public static final Texture TX_WIZARD= new Texture("characters/wizard.png");
	public static final Texture TX_BLACKSOLDIER = new Texture("characters/blacksoldier.png");
	public static final Texture TX_HARUSAME = new Texture("characters/harusame.png");
	public static final Texture TX_CAULFIELD = new Texture("characters/caulfield.png");
	public static final Texture TX_FLANE = new Texture("characters/flane.png");
	public static final Texture TX_RELM = new Texture("characters/relm.png");
	public static final Texture TX_GARYWISS = new Texture("characters/garywiss.png");
	
	public static final Texture TX_SLIME_01 = new Texture("characters/slime01.png");
	public static final Texture TX_SLIME_02 = new Texture("characters/slime02.png");
	public static final Texture TX_SLIME_03 = new Texture("characters/slime03.png");
	public static final Texture TX_SLIME_04 = new Texture("characters/slime04.png");
	
	public static final Texture TX_LAVAROCK_01 = new Texture("characters/lavarock_01.png");
	public static final Texture TX_LAVAROCK_02 = new Texture("characters/lavarock_02.png");
	
	public static final TextureRegion[][] TX_WIND = TextureRegion.split(new Texture("characters/wind.png"),32,32);
	
	// MAPS
	public static final Texture MAP_CLOUD_01= new Texture("map_animations/cloud01.png");
	public static final Texture MAP_CLOUD_02= new Texture("map_animations/cloud02.png");
	public static final Texture MAP_CLOUD_03= new Texture("map_animations/cloud03.png");
	
	public static final Texture MAP_CLOUD_04= new Texture("map_animations/cloud04.png");
	public static final Texture MAP_CLOUD_05= new Texture("map_animations/cloud05.png");
	public static final Texture MAP_CLOUD_06= new Texture("map_animations/cloud06.png");
	
	public static final TextureRegion[][] MAP_METEORITE= TextureRegion.split(new Texture("map_animations/animated_meteorite.png"),16,16);
	
	public static final Texture MAPITEM_FLOWER= new Texture("map_animations/animated_flower.png");
	public static final Texture MAPITEM_LAVA= new Texture("map_animations/animated_lava.png");

	public static final TextureRegion[][] MAPITEM_METEORITE= TextureRegion.split(new Texture("items/meteorite.png"),32,32);

	
	public static final Texture MAP01_SKY = new Texture("maps/map01_sky.png");
	public static final Texture MAP02_SKY = new Texture("maps/map02_sky.png");
	
	public static final Texture MAP01_BACKGROUND = new Texture("maps/map01_background.png");
	public static final Texture MAP02_BACKGROUND = new Texture("maps/map02_background.png");
	
	public static final Texture MAP01_BACK = new Texture("maps/map01_back.png");
	public static final Texture MAP01_MASK = new Texture("maps/map01_mask.png");
	
	public static final Texture MAP02_BACK = new Texture("maps/map02_back.png");
	public static final Texture MAP02_MASK = new Texture("maps/map02_mask.png");

	// MAPS ITEMS
	public static final TextureRegion[][] MAP_ITEM_ICONS = TextureRegion.split(new Texture("weapons/icons.png"),32,32);
	public static final TextureRegion[][] MAP_ITEM_APPEAR = TextureRegion.split(new Texture("map_animations/item_appear.png"),32,32);
	
	// WEAPONS
	public static final TextureRegion[][] WEAPON_SHOVEL = TextureRegion.split(new Texture("weapons/shovel.png"),64,64);
	public static final TextureRegion[][] WEAPON_HAMMER = TextureRegion.split(new Texture("weapons/hammer.png"),64,64);

	// ANIMATIONS
	public static final TextureRegion[][] WEAPON_HIT = TextureRegion.split(new Texture("map_animations/hit.png"),16,16);
	public static final TextureRegion[][] ANIM_FEATHER = TextureRegion.split(new Texture("map_animations/feather.png"),16,16);
	public static final TextureRegion[][] ANIM_IRON_POTION = TextureRegion.split(new Texture("map_animations/iron_potion.png"),16,16);
	public static final TextureRegion[][] ANIM_EXPLOSION = TextureRegion.split(new Texture("map_animations/explosion.png"),64,64);
			 
			 
	/*
	 * SOUNDS 
	 */
	public static final Sound SOUND_BIP01 = Gdx.audio.newSound(Gdx.files.internal("sounds/bip01.wav"));
	public static final Sound SOUND_SELEC01  = Gdx.audio.newSound(Gdx.files.internal("sounds/select01.wav"));
	public static final Sound SOUND_PUSH_HEROS  = Gdx.audio.newSound(Gdx.files.internal("sounds/push_heros.wav"));
	public static final Sound SOUND_PUSH_SLIME  = Gdx.audio.newSound(Gdx.files.internal("sounds/push_slime.wav"));
	public static final Sound SOUND_FALL  = Gdx.audio.newSound(Gdx.files.internal("sounds/fall.wav"));
	public static final Sound SOUND_PICKUP  = Gdx.audio.newSound(Gdx.files.internal("sounds/pickup.wav"));
	
	public static final Sound SOUND_TAKE_POTION  = Gdx.audio.newSound(Gdx.files.internal("sounds/drink_potion.wav"));
	public static final Sound SOUND_TAKE_FEATHER = Gdx.audio.newSound(Gdx.files.internal("sounds/take_leather.wav"));
	public static final Sound SOUND_TAKE_WEAPON = Gdx.audio.newSound(Gdx.files.internal("sounds/take_weapons.wav"));
	
	public static final Sound SOUND_APPEAR = Gdx.audio.newSound(Gdx.files.internal("sounds/appear.wav"));
	
	public static final Sound SOUND_METEORITE_EXPLOSE = Gdx.audio.newSound(Gdx.files.internal("sounds/meteorite_explosion.wav"));
	public static final Sound SOUND_METEORITE_FALL = Gdx.audio.newSound(Gdx.files.internal("sounds/meteorite_fall.wav"));
	
	/*
	 * MUSICS
	 */
	public static final Music MUSIC_01 = Gdx.audio.newMusic(Gdx.files.internal("musics/music01.mp3"));
}
