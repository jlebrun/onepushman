package com.jln.ld;

import com.jln.ld.menus.GameMenu;

public class GamePoint {

	float x;
	float y;
	
	public GamePoint(float x, float y){
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Dessine le point
	 * @param game
	 */
	public void render(GameMenu game){
		GameShared.instance.gameBatch.draw(GameConstant.DEBUG_POINT, x-4, y-4);
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	
}
