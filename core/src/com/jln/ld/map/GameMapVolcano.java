package com.jln.ld.map;

import java.util.ArrayList;
import java.util.List;

import com.jln.ld.GameConstant;
import com.jln.ld.GamePoint;
import com.jln.ld.GameShared;
import com.jln.ld.characters.LavaRock;
import com.jln.ld.characters.Slime;
import com.jln.ld.menus.GameMenu;

public class GameMapVolcano extends GameMap{

	private List<MapMeteoriteAnimation> mapMeteorites;
	private List<MapMeteoriteImpact> mapMeteoriteImpacts;

	private int ennemyDie = 0;
	/**
	 * Constructeur
	 * @param mapCode
	 */
	public GameMapVolcano(GameMenu game,int mapCode) {
		super(game,mapCode);
	}
	
	/**
	 * Initialise les ressources
	 */
	public void initRessources(GameMenu game){
		super.initRessources(game);
		mapMeteorites = new ArrayList<MapMeteoriteAnimation>();
		this.mapMeteorites.add(new MapMeteoriteAnimation(mapCode));
		this.mapMeteorites.add(new MapMeteoriteAnimation(mapCode));
		this.mapMeteorites.add(new MapMeteoriteAnimation(mapCode));
		this.mapMeteorites.add(new MapMeteoriteAnimation(mapCode));
		this.mapMeteorites.add(new MapMeteoriteAnimation(mapCode));
		
		mapMeteoriteImpacts = new ArrayList<MapMeteoriteImpact>();

	}
	
	/**
	 * Dessine la carte
	 */
	public void render(GameMenu game){
		super.render(game);
	}
	
	/**
	 * Dessine les formes
	 */
	public void renderShape(GameMenu game){
		for(MapMeteoriteImpact  m: mapMeteoriteImpacts){
			m.renderShape(game);
		}
	}
	
	/**
	 * Dessine l'arriere plan
	 * @param game
	 */
	public void renderBackground(GameMenu game){
		GameShared.instance.gameBatch.draw(GameConstant.MAP02_SKY, 0,0);
		for(MapMeteoriteAnimation  m: mapMeteorites){
			m.render(game);
		}
		GameShared.instance.gameBatch.draw(GameConstant.MAP02_BACKGROUND, 0,0);
		
		for(MapCloud c : clouds){
			c.render(game);
		}
	}
	
	/**
	 * Dessine la map
	 * @param game
	 */
	public void renderFront(GameMenu game){
		super.renderFront(game);
		
		for(MapMeteoriteImpact  m: mapMeteoriteImpacts){
			m.render(game);
		}
	}
	
	/**
	 * Met a jour la carte
	 */
	public void update(GameMenu game){
		super.update(game);
		

		for(MapMeteoriteAnimation  m: mapMeteorites){
			m.update(game);
		}
		for(MapMeteoriteImpact  m: mapMeteoriteImpacts){
			m.update(game);
		}
	}
	
	/**
	 * Methode appel�e quand un ennemi meurt
	 */
	@Override
	public void ennemyDie(GameMenu game){
		ennemyDie++;
		
		
		if(ennemyDie%7==0 && mapMeteoriteImpacts.size()<7){
			this.mapMeteoriteImpacts.add(new MapMeteoriteImpact(game,mapCode));
		}
		
		int nbEnnemies = game.getCharacters().size();
		if(ennemyDie <12 && nbEnnemies>5){
			return ;
		}else if(ennemyDie < 20 && nbEnnemies>8){
			return;
		}else if(ennemyDie < 30 && nbEnnemies>10){
			return;
		}else if(ennemyDie < 40 && nbEnnemies>12){
			return;
		}else if(nbEnnemies > 20){
			return;
		}

		double r = Math.random();
		if(ennemyDie < 10){
			this.addRandomEnemy(game);
			if(r<0.25 || (ennemyDie%3==0)){
				// On en ajoute un deuxieme
				this.addRandomEnemy(game);
			}
		}else if(ennemyDie < 30){
			this.addRandomEnemy(game);
			if(r<0.25){
				this.addRandomEnemy(game);
				if(r<0.1){
					this.addRandomEnemy(game);
				}
			}
		}else if(ennemyDie < 70){
			this.addRandomEnemy(game);
			if(r<0.5){
				this.addRandomEnemy(game);
				if(r<0.25){
					this.addRandomEnemy(game);
					if(r<0.1){
						this.addRandomEnemy(game);
					}
				}
			}
		}else{
			this.addRandomEnemy(game);
			this.addRandomEnemy(game);
			this.addRandomEnemy(game);
		}

	}

	/**
	 * Ajoute un slime aleatoirement.
	 * @param game
	 */
	public void addRandomEnemy(GameMenu game){
		double r = Math.random();
		GamePoint p = this.getRandomTargetPoint();
		int enemyType = -1;
		if(ennemyDie == 0){
			enemyType = LavaRock.BASIC_LAVAROCK;
		}else if(ennemyDie < 10){
			if(r<0.5){
				enemyType = LavaRock.BASIC_LAVAROCK;
			}else if(r<0.75){
				enemyType = LavaRock.BASIC_LAVAROCK;
			}else{
				enemyType = LavaRock.BIG_LAVAROCK;
			}
		}else if(ennemyDie < 25){
			if(r<0.25){
				enemyType = LavaRock.BASIC_LAVAROCK;
			}else if(r<0.5){
				enemyType = LavaRock.BASIC_LAVAROCK;
			}else if(r<0.75){
				enemyType = LavaRock.BASIC_LAVAROCK;
			}else{
				enemyType = LavaRock.BIG_LAVAROCK;
			}
		}else{
			if(r<0.15){
				enemyType = LavaRock.BASIC_LAVAROCK;
			}else if(r<0.3){
				enemyType = LavaRock.BASIC_LAVAROCK;
			}else if(r<0.7){
				enemyType = LavaRock.BIG_LAVAROCK;
			}else{
				enemyType = LavaRock.BIG_LAVAROCK;
			}
		}
		game.getCharacters().add(new LavaRock(p.getX(),p.getY(), enemyType));
	}

}
