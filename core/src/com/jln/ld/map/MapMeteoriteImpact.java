package com.jln.ld.map;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.jln.ld.GameAnimation;
import com.jln.ld.GameConstant;
import com.jln.ld.GameEntity;
import com.jln.ld.GamePoint;
import com.jln.ld.GameShared;
import com.jln.ld.GameUtils;
import com.jln.ld.characters.GameCharacter;
import com.jln.ld.menus.GameMenu;

public class MapMeteoriteImpact extends GameEntity{

	protected List<GamePoint> oldPositions;
	protected float stateTime;
	protected GamePoint targetPoint;
	protected int cptWait;
	protected int cptWarning = 60;
	protected float x,y;
	protected float speed;

	protected Animation warningAnimation;
	protected TextureRegion meteoriteFrame;

	protected int cptFrame = 0;
	public static Animation[] animationExplosions;

	protected Rectangle collision;


	/**
	 * Constructeur
	 */
	public MapMeteoriteImpact(GameMenu game, int mapCode){
		this.oldPositions = new ArrayList<GamePoint>();

		warningAnimation = GameUtils.loadAnimation(GameConstant.MAPITEM_METEORITE, 4, 0, 0.1f);
		warningAnimation.setPlayMode(PlayMode.LOOP);
		meteoriteFrame = GameConstant.MAPITEM_METEORITE[1][0];

		cptWait = (int)(Math.random()*600);

		if(animationExplosions == null){
			animationExplosions = new Animation[9];
			for(int i=0;i<9;i++){
				animationExplosions[i] = GameUtils.loadAnimation(GameConstant.ANIM_EXPLOSION, 6, i, 0.08f);
			}
		}


	}

	/**
	 * Initialise la meteorite
	 * @param game
	 */
	private void init(GameMenu game){
		targetPoint = game.getMap().getRandomTargetPoint();
		cptWarning = 90;
		y = 500;
		x = targetPoint.getX();
		speed = (float) (2+(Math.random()*2));
		this.oldPositions.clear();

		collision = new Rectangle(targetPoint.getX()-32, targetPoint.getY()-32,64,64);
		GameConstant.SOUND_METEORITE_FALL.play();
		
	}

	/**
	 * Dessine la meteorite
	 */
	public void render(GameMenu game){
		if(cptWait<=0){
			if(cptWarning>0){
				GameShared.instance.gameBatch.draw(warningAnimation.getKeyFrame(stateTime), targetPoint.getX()-16, targetPoint.getY()-16);
			}else if((y-targetPoint.getY())<48){
				GameShared.instance.gameBatch.draw(GameConstant.MAPITEM_METEORITE[2][3], targetPoint.getX()-16, targetPoint.getY()-16);

			}else if((y-targetPoint.getY())<96){
				GameShared.instance.gameBatch.draw(GameConstant.MAPITEM_METEORITE[2][2], targetPoint.getX()-16, targetPoint.getY()-16);

			}else if((y-targetPoint.getY())<144){
				GameShared.instance.gameBatch.draw(GameConstant.MAPITEM_METEORITE[2][1], targetPoint.getX()-16, targetPoint.getY()-16);

			}else if((y-targetPoint.getY())<192){
				GameShared.instance.gameBatch.draw(GameConstant.MAPITEM_METEORITE[2][1], targetPoint.getX()-16, targetPoint.getY()-16);
			}
			if(!oldPositions.isEmpty()){
				GamePoint p;
				float color = 1f;
				for(int i=oldPositions.size()-1;i>=0;i--){
					p = oldPositions.get(i);
					color = 1-(i*0.05f);
					GameShared.instance.gameBatch.setColor(color,color,color,color );
					GameShared.instance.gameBatch.draw(meteoriteFrame, p.getX()-16, p.getY()-16);
				}
			}
			GameShared.instance.gameBatch.setColor(1, 1, 1, 1);
			GameShared.instance.gameBatch.draw(meteoriteFrame, x-16, y-16);

			stateTime += Gdx.graphics.getDeltaTime(); 
		}
	}

	/**
	 * Dessine les collisions des meteorites
	 * @param game
	 */
	public void renderShape(GameMenu game){
		if(collision != null && cptWait<=0){
			GameShared.instance.shapeRenderer.setColor(1, 0, 0, 1);
			GameShared.instance.shapeRenderer.rect(collision.x, collision.y, collision.width, collision.height);
		}
	}

	/**
	 * Met a jour la meteorite
	 */
	public void update(GameMenu game){
		if(cptWait>0){
			cptWait--;
			if(cptWait==0){
				this.init(game);
			}
		}else{
			if(cptWarning>0){
				cptWarning--;
			}else{
				if(y>targetPoint.getY()){
					if(cptFrame==0){
						if(oldPositions.size()>=20){
							oldPositions.remove(19);
						}
						oldPositions.add(0, new GamePoint(x,y));
						cptFrame = (int)(4-speed);
					}else{
						cptFrame--;
					}
					y-=speed;
					if(y<=targetPoint.getY()){
						this.explode(game);

					}
				}
			}
		}
	}

	/**
	 * Impact de la méteorite sur le sol.
	 * @param game
	 */
	private void explode(GameMenu game){

		// Ajout des explosions
		int nbExplosion = (int)(2+Math.random()*3);
		float ex, ey;
		int coldown = 0;
		game.addAnimation(new GameAnimation(animationExplosions[0], 64, (targetPoint.getX()-16), (targetPoint.getY()-16), 0));
		for(int i=0;i<nbExplosion;i++){
			ex = (float) ((targetPoint.getX()-16) + ((Math.random()*64) - 32));
			ey = (float) ((targetPoint.getY()-16) + ((Math.random()*64) - 32));
			coldown = (int)(Math.random()*60);
			game.addAnimation(new GameAnimation(animationExplosions[(int)(Math.random()*8)+1], 64, ex, ey, coldown ));
		}

		// Ejection des persos
		for(GameCharacter c : game.getCharacters()){
			if(c.onContact(collision)){
				float factorX = x>c.getX()?-1:1;
				float factorY = y>c.getY()?-1:1;

				float a = (c.getY()-y)/(c.getX()-x);
				if(Math.abs(a)<0.5){
					factorY = 0;
				}else if(Math.abs(a)>2){
					factorX = 0;
				}
				double dist = Math.sqrt(Math.pow((x-c.getX()),2)+Math.pow((y-c.getY()),2));
				float power = ((float) Math.abs(64 - dist))+3;
				if(power>32){
					power = 32;
				}
				c.pushByItem(game, power*factorX, power*factorY);
			}
		}
		
		GameConstant.SOUND_METEORITE_EXPLOSE.play();

		// Reinitialisation de la meteorite
		cptWait = (int)(Math.random()*600)+5;	
	}
}
