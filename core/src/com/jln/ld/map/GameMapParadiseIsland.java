package com.jln.ld.map;


import com.jln.ld.GameConstant;
import com.jln.ld.GamePoint;
import com.jln.ld.GameShared;
import com.jln.ld.characters.Slime;
import com.jln.ld.menus.GameMenu;

public class GameMapParadiseIsland extends GameMap{

	private int ennemyDie = 0;

	/**
	 * Constructeur
	 * @param mapCode
	 */
	public GameMapParadiseIsland(GameMenu game,int mapCode) {
		super(game,mapCode);
	}

	/**
	 * Initialise les ressources
	 */
	public void initRessources(GameMenu game){
		super.initRessources(game);

	}

	/**
	 * Dessine la carte
	 */
	public void render(GameMenu game){
		super.render(game);
	}

	/**
	 * Dessine les formes
	 */
	public void renderShape(GameMenu game){

	}

	/**
	 * Dessine l'arriere plan
	 * @param game
	 */
	public void renderBackground(GameMenu game){
		GameShared.instance.gameBatch.draw(GameConstant.MAP01_SKY, 0,0);
		GameShared.instance.gameBatch.draw(GameConstant.MAP01_BACKGROUND, 0,0);

		for(MapCloud c : clouds){
			c.render(game);
		}
	}

	/**
	 * Dessine la map
	 * @param game
	 */
	public void renderFront(GameMenu game){
		super.renderFront(game);

	}

	/**
	 * Met a jour la carte
	 */
	public void update(GameMenu game){
		super.update(game);
	}

	/**
	 * Methode appel�e quand un ennemi meurt
	 */
	@Override
	public void ennemyDie(GameMenu game){
		// a redefinir
		ennemyDie++;
		
		int nbEnnemies = game.getCharacters().size();
		if(ennemyDie <12 && nbEnnemies>5){
			return ;
		}else if(ennemyDie < 20 && nbEnnemies>8){
			return;
		}else if(ennemyDie < 30 && nbEnnemies>10){
			return;
		}else if(ennemyDie < 40 && nbEnnemies>12){
			return;
		}else if(nbEnnemies > 20){
			return;
		}

		double r = Math.random();
		if(ennemyDie < 10){
			this.addRandomSlime(game);
			if(r<0.25 || (ennemyDie%3==0)){
				// On en ajoute un deuxieme
				this.addRandomSlime(game);
			}
		}else if(ennemyDie < 30){
			this.addRandomSlime(game);
			if(r<0.25){
				this.addRandomSlime(game);
				if(r<0.1){
					this.addRandomSlime(game);
				}
			}
		}else if(ennemyDie < 70){
			this.addRandomSlime(game);
			if(r<0.5){
				this.addRandomSlime(game);
				if(r<0.25){
					this.addRandomSlime(game);
					if(r<0.1){
						this.addRandomSlime(game);
					}
				}
			}
		}else{
			this.addRandomSlime(game);
			this.addRandomSlime(game);
			this.addRandomSlime(game);
		}

	}

	/**
	 * Ajoute un slime aleatoirement.
	 * @param game
	 */
	public void addRandomSlime(GameMenu game){
		
		double r = Math.random();
		GamePoint p = this.getRandomTargetPoint();
		int slimeType = -1;
		if(ennemyDie == 0){
			slimeType = Slime.BASIC_SLIME;
		}else if(ennemyDie < 10){
			if(r<0.5){
				slimeType = Slime.BASIC_SLIME;
			}else if(r<0.75){
				slimeType = Slime.SLOW_SLIME;
			}else{
				slimeType = Slime.AGGRO_SLIME;
			}
		}else if(ennemyDie < 25){
			if(r<0.25){
				slimeType = Slime.BASIC_SLIME;
			}else if(r<0.5){
				slimeType = Slime.SLOW_SLIME;
			}else if(r<0.75){
				slimeType = Slime.AGGRO_SLIME;
			}else{
				slimeType = Slime.BIG_SLIME;
			}
		}else{
			if(r<0.15){
				slimeType = Slime.BASIC_SLIME;
			}else if(r<0.3){
				slimeType = Slime.SLOW_SLIME;
			}else if(r<0.7){
				slimeType = Slime.AGGRO_SLIME;
			}else{
				slimeType = Slime.BIG_SLIME;
			}
		}
		game.getCharacters().add(new Slime(p.getX(),p.getY(), slimeType));
	}

}
