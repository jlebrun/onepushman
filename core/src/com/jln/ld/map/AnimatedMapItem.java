package com.jln.ld.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.jln.ld.GameConstant;
import com.jln.ld.GameEntity;
import com.jln.ld.GameShared;
import com.jln.ld.GameUtils;
import com.jln.ld.menus.GameMenu;

public class AnimatedMapItem extends GameEntity{

	public static final int ITEM_FLOWER = 0;
	public static final int ITEM_LAVA = 1;
	private int type;
	private Animation frames;
	protected float stateTime = 0f;
	protected float x, y;
	
	protected int cptPause;

	
	/**
	 * Constructeur
	 */
	public AnimatedMapItem(int type,float x, float y){
		this.type = type;
		this.x = x;
		this.y = y;
		if(type == ITEM_FLOWER || type == ITEM_LAVA){
			TextureRegion[][] tmp = TextureRegion.split(type == ITEM_FLOWER?GameConstant.MAPITEM_FLOWER:GameConstant.MAPITEM_LAVA, 16, 16);
			frames = GameUtils.loadAnimation(tmp, 4, (int)(Math.random()*4), type == ITEM_FLOWER?0.26f:0.15f);
			frames.setPlayMode(PlayMode.LOOP);
			stateTime = (float) (Math.random()*5);
		}
	}
	
	/**
	 * 
	 */
	public void update(GameMenu game){
		if(cptPause > 0){
			cptPause--;
		}else{
			stateTime += Gdx.graphics.getDeltaTime(); 
			if(type == ITEM_FLOWER){
				if(Math.random()<0.02){
					cptPause = 35;
				}
			}
		}
	}
	
	/**
	 * 
	 */
	public void render(GameMenu game){
		if(frames != null){
			GameShared.instance.gameBatch.draw(frames.getKeyFrame(stateTime), x-8,y-8);
		}
	}
}
