package com.jln.ld.map;

import com.badlogic.gdx.graphics.Texture;
import com.jln.ld.GameConstant;
import com.jln.ld.GameEntity;
import com.jln.ld.GameShared;
import com.jln.ld.menus.GameMenu;

public class MapCloud extends GameEntity{
	protected float x,y;
	protected Texture texture;
	protected float speed = 0.2f;
	
	protected boolean onFront=false;
	
	/**
	 * Constructeur
	 */
	public MapCloud(boolean onFront, int mapCode){
		this.x = (float) (Math.random()*1600);
		this.y = (float) (-120+(Math.random()*(onFront?70:260)));
		this.onFront = onFront;
		
		int r = (int)(Math.random()*3);
		if(r==0){
			texture = mapCode==1?GameConstant.MAP_CLOUD_01:GameConstant.MAP_CLOUD_04;

		}else if(r==1){
			texture =  mapCode==1?GameConstant.MAP_CLOUD_02:GameConstant.MAP_CLOUD_05;

		}else{
			texture = mapCode==1?GameConstant.MAP_CLOUD_03:GameConstant.MAP_CLOUD_06;
		}
		
		speed += (Math.random()*(onFront?0.5:0.3f));
		
	}
	
	/**
	 * Dessine le nuage
	 */
	@Override
	public void render(GameMenu game){
		if(x>-500 && x<640){
			GameShared.instance.gameBatch.setColor(1f, 1f, 1f, (onFront?0.5f:1f));
			GameShared.instance.gameBatch.draw(texture, x, y);
			GameShared.instance.gameBatch.setColor(1f, 1f, 1f, 1f);

		}
	}
	
	/**
	 * Met a jour le nuage
	 */
	@Override
	public void update(GameMenu game){
		if(x < -600){
			x = (float) (640+(Math.random()*1000));
			y = (float) (-120+(Math.random()*260));
		}else{
			x-=speed;
		}
	}
}
