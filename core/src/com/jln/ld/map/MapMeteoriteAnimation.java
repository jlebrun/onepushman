package com.jln.ld.map;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.jln.ld.GameConstant;
import com.jln.ld.GameEntity;
import com.jln.ld.GamePoint;
import com.jln.ld.GameShared;
import com.jln.ld.GameUtils;
import com.jln.ld.menus.GameMenu;

public class MapMeteoriteAnimation extends GameEntity{
	protected float x,y;
	protected Animation meteoriteAnimation;
	protected float dx=0f,dy=0f;
	protected float stateTime =0f;
	protected int cptWait = 0;
	private int cptFrame = 0;

	protected List<GamePoint> oldPositions;

	/**
	 * Constructeur
	 */
	public MapMeteoriteAnimation(int mapCode){
		this.oldPositions = new ArrayList<GamePoint>();

		this.init();

	}

	private void init(){
		int caseY = (int)(Math.random()*8);
		meteoriteAnimation = GameUtils.loadAnimation(GameConstant.MAP_METEORITE, 4,caseY, 0.09f);
		meteoriteAnimation.setPlayMode(PlayMode.LOOP);

		this.x = (float) (Math.random()*640);
		this.y =  400f;
		dy = (float) -(1+(Math.random()*4));
		dx = (float) (Math.random()*2)-1;
		cptWait = (int)(Math.random()*700);

		if(caseY<3 && dy<-3){
			dy/=2;
		}
	}

	/**
	 * Dessine le nuage
	 */
	@Override
	public void render(GameMenu game){
		if(cptWait<=0){
			GamePoint mp;
			if(!oldPositions.isEmpty()){
				for(int i=oldPositions.size()-1;i>=0;i--){
					mp = oldPositions.get(i);
					GameShared.instance.gameBatch.setColor(1, 1, 1, 1-(i*0.1f));
					GameShared.instance.gameBatch.draw(meteoriteAnimation.getKeyFrame(stateTime), mp.getX(), mp.getY());
				}
			}
			GameShared.instance.gameBatch.setColor(1, 1, 1, 1f);
			GameShared.instance.gameBatch.draw(meteoriteAnimation.getKeyFrame(stateTime), x, y);
			stateTime += Gdx.graphics.getDeltaTime(); 
		}
	}

	
	/**
	 * Met a jour le nuage
	 */
	@Override
	public void update(GameMenu game){
		if(cptWait>0){
			cptWait--;
		}else{
			if(y<-15){
				this.init();
			}else{
				if(oldPositions.size()>=10){
					oldPositions.remove(9);
				}
				oldPositions.add(0, new GamePoint(x,y));
				this.x+=dx;
				this.y+=dy;
			}
		}
	}
}
