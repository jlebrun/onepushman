package com.jln.ld.map;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.jln.ld.GameConstant;
import com.jln.ld.GameEntity;
import com.jln.ld.GamePoint;
import com.jln.ld.GameShared;
import com.jln.ld.menus.GameMenu;

public abstract class GameMap extends GameEntity{

	protected int mapCode;
	protected Texture backgroundTexture;
	protected Texture maskTexture;
	protected int[][] collision;
	protected List<MapCloud> clouds, frontClouds;
	protected List<AnimatedMapItem> mapItems;

	/**
	 * Constructeur
	 * @param mapCode
	 */
	public GameMap(GameMenu game,int mapCode){
		this.mapCode = mapCode;
		this.initRessources(game);
	}

	/**
	 * Initialise les ressources de la map
	 */
	public void initRessources(GameMenu game){
		if(mapCode == 1){
			backgroundTexture = GameConstant.MAP01_BACK;
			maskTexture = GameConstant.MAP01_MASK;
		}else if(mapCode == 2){
			backgroundTexture = GameConstant.MAP02_BACK;
			maskTexture = GameConstant.MAP02_MASK;
		}

		Pixmap pixmap;
		Color color;

		pixmap = new Pixmap(Gdx.files.internal("maps/map0"+mapCode+"_mask.png"));
		color = new Color();
		int val;

		collision = new int[72][128];
		for(int i=0;i<72;i++){
			for(int j=0;j<128;j++){
				val = pixmap.getPixel(j, i);
				Color.rgba8888ToColor(color, val);
				int r = (int)(color.r * 255f);
				int g = (int)(color.g * 255f);
				int b = (int)(color.b * 255f);
				if(r==255 && g==0 && b==0){
					collision[i][j] = 2;
				}else if(r==0 && g==0 && b==255){
					collision[i][j] = 3;
				}else if(r==0 && g==0 && b==0){
					collision[i][j] = 1;
				}else if(r==200 && g==200 && b==200){
					collision[i][j] = 3;
				}else{
					collision[i][j] = 0;
				}
			}
		}

		this.clouds = new ArrayList<MapCloud>();
		this.frontClouds = new ArrayList<MapCloud>();

		for(int i=0;i<6;i++){
			this.clouds.add(new MapCloud(false, mapCode));
		}
		for(int i=0;i<3;i++){
			this.frontClouds.add(new MapCloud(true, mapCode));
		}

		this.mapItems = new ArrayList<AnimatedMapItem>();

		if(mapCode == 1){
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_FLOWER,140,260));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_FLOWER,120,120));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_FLOWER,270,230));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_FLOWER,430,110));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_FLOWER,520,220));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_FLOWER,180,250));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_FLOWER,310,129));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_FLOWER,290,239));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_FLOWER,410,125));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_FLOWER,500,240));
		}else if(mapCode == 2){
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_LAVA,107,222));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_LAVA,303,146));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_LAVA,212,172));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_LAVA,261,220));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_LAVA,203,236));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_LAVA,365,238));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_LAVA,383,159));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_LAVA,463,157));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_LAVA,474,242));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_LAVA,524,245));
			this.mapItems.add(new AnimatedMapItem(AnimatedMapItem.ITEM_LAVA,537,275));
		}
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @return le type de terrain : 0 = passable, 1 = bloqu�, 2=vide
	 */
	public int getMapState(float x, float y){
		int i = (int)y;
		int j = (int)x;
		i = i/5;
		j = j/5;
		if(i>71){
			i=71;
		}
		if(j<1){
			j=1;
		}
		if(j>127){
			j=127;
		}
		if(i<0){
			i = 0;
		}
		return collision[71-(i)][j-1];
	}
	/**
	 * Maj la map
	 * @param game
	 */
	public void update(GameMenu game){
		for(MapCloud c : clouds){
			c.update(game);
		}
		for(MapCloud c : frontClouds){
			c.update(game);
		}
		for(AnimatedMapItem  a: mapItems){
			a.update(game);
		}
		
	}

	/**
	 * 
	 * @return
	 */
	public GamePoint getRandomTargetPoint(){
		GamePoint p = new GamePoint((float)Math.random()*640,(float)Math.random()*360);
		while(getMapState(p.getX(),p.getY())!=0){
			p.setX((float)Math.random()*GameConstant.GAME_WIDTH);
			p.setY((float)Math.random()*GameConstant.GAME_HEIGHT);
		}
		return p;
	}

	/**
	 * Dessine l'arriere plan
	 * @param game
	 */
	public void renderBackground(GameMenu game){
		
	}

	/**
	 * Dessine la map
	 * @param game
	 */
	public void render(GameMenu game){
		if(backgroundTexture != null){
			GameShared.instance.gameBatch.draw(backgroundTexture, 0,0);
			for(AnimatedMapItem  a: mapItems){
				a.render(game);
			}
//			GameShasred.instance.gameBatch.draw(maskTexture, 0,0);
		}
	}

	/**
	 * Dessine la map
	 * @param game
	 */
	public void renderFront(GameMenu game){
		for(MapCloud c : frontClouds){
			c.render(game);
		}
	}
	

	/**
	 * Dessine les formes
	 */
	public void renderShape(GameMenu game){

	}
	
	public int getMapCode(){
		return mapCode;
	}
	
	public void ennemyDie(GameMenu game){
		// a redefinir
	}
}
