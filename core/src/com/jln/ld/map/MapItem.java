package com.jln.ld.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jln.ld.GameConstant;
import com.jln.ld.GameEntity;
import com.jln.ld.GameShared;
import com.jln.ld.GameUtils;
import com.jln.ld.characters.GameCharacter;
import com.jln.ld.menus.GameMenu;

public class MapItem extends GameEntity{

	public static final int NO_ITEM = -1;
	public static final int WEAPON_SHOVEL = 0;
	public static final int WEAPON_HAMMER = 1;
	public static final int ITEM_FEATHER = 2;
	public static final int ITEM_IRON_POTION = 3;
	public static final int CELL_SIZE = 32;
	
	private int type;
	private TextureRegion frame;
	protected float x, y;
	
	protected int cptPause;
	
	protected boolean taken = false;
	protected boolean canBeDestroy = false;
	
	protected int timer = 600;
	
	protected Animation appearAnimation;
	protected boolean appearPlay = false;
	protected float stateTime = 0f;

	/**
	 * Constructeur
	 */
	public MapItem(int type,float x, float y){
		this.type = type;
		this.x = x;
		this.y = y;
		if(type == WEAPON_SHOVEL){
			frame = GameConstant.MAP_ITEM_ICONS[0][0];
		}else if(type == WEAPON_HAMMER){
			frame = GameConstant.MAP_ITEM_ICONS[0][1];
		}else if(type == ITEM_FEATHER){
			frame = GameConstant.MAP_ITEM_ICONS[1][0];
		}else if(type == ITEM_IRON_POTION){
			frame = GameConstant.MAP_ITEM_ICONS[1][1];
		}
		
		// appear 
		appearAnimation = GameUtils.loadAnimation(GameConstant.MAP_ITEM_APPEAR, 8, 0, 0.07f);
		appearAnimation.setPlayMode(PlayMode.NORMAL);
		appearPlay = false;
		stateTime = 0f;
	}
	
	/**
	 * 
	 */
	public void update(GameMenu game){
		if(!taken && !canBeDestroy && appearPlay){
			for(GameCharacter c : game.getCharacters()){
				if(c.getCollision().contains(x, y) && c.isActive()){
					taken = true;
					canBeDestroy = true;
					if(type == WEAPON_SHOVEL || type == WEAPON_HAMMER){
						GameConstant.SOUND_TAKE_WEAPON.play();
					}else if(type== ITEM_FEATHER){
						GameConstant.SOUND_TAKE_FEATHER.play();
					}else if(type== ITEM_IRON_POTION){
						GameConstant.SOUND_TAKE_POTION.play();
					}
					c.addItem(type);
				}
			}
			timer --;
			if(timer<=0){
				this.canBeDestroy = true;
			}
		}
	}
	
	/**
	 * 
	 */
	public void render(GameMenu game){
		if(!appearPlay){
			stateTime += Gdx.graphics.getDeltaTime(); 
			if(appearAnimation.isAnimationFinished(stateTime)){
				appearPlay = true;
			}else{
				GameShared.instance.gameBatch.draw(appearAnimation.getKeyFrame(stateTime), x-16, y-16);
			}
		}else{
			if(timer<100){
				GameShared.instance.gameBatch.setColor(1f, 1f, 1f, ((float)(timer)/100.0f));
			}
			GameShared.instance.gameBatch.draw(frame, x-(CELL_SIZE/2), y-(CELL_SIZE/2));
			if(timer<100){
				GameShared.instance.gameBatch.setColor(1f, 1f, 1f, 1f);
			}
		}
		
	}
	

	public boolean isCanBeDestroy() {
		return canBeDestroy;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	
	
}
