package com.jln.ld.menus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.jln.ld.GameShared;
import com.jln.ld.GameAnimation;
import com.jln.ld.GameConstant;
import com.jln.ld.GameHud;
import com.jln.ld.GamePoint;
import com.jln.ld.Ld38Launcher;
import com.jln.ld.characters.GameCharacter;
import com.jln.ld.characters.Heros;
import com.jln.ld.characters.LavaRock;
import com.jln.ld.characters.Slime;
import com.jln.ld.characters.Wind;
import com.jln.ld.map.GameMap;
import com.jln.ld.map.GameMapParadiseIsland;
import com.jln.ld.map.GameMapVolcano;
import com.jln.ld.map.MapItem;

public class GameMenu {

	////////////////////////////////////////////////////////////////////////////////////
	// ATTRIBUTS                                                                      //
	////////////////////////////////////////////////////////////////////////////////////
	
	private GameHud hud;
	private Heros heros;
	private GameMap map;
	private int cptGameOver = 0;
	private List<GameCharacter> characters;
	private List<MapItem> items;
	private List<GameAnimation> animations;
	private List<Wind> winds;
	
	
	private boolean needUpdate = false;
	
	private int waveNo = 0;
	
	////////////////////////////////////////////////////////////////////////////////////
	// METHODES                                                                       //
	////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Constructeur
	 */
	public GameMenu(){
//		this.init();
	}
	
	/**
	 * Initialise les ressources
	 */
	public void init(){
		this.hud = new GameHud();
		this.heros = new Heros();
		if(GameShared.instance.levelNumber  == 1){
			this.map = new GameMapParadiseIsland(this,GameShared.instance.levelNumber );
		}else if(GameShared.instance.levelNumber  == 2){
			this.map = new GameMapVolcano(this,GameShared.instance.levelNumber );
		}
		
		this.cptGameOver = 0;
		
		this.characters = new ArrayList<GameCharacter>();
		this.characters.add(heros);
		
		items = new ArrayList<MapItem>();
		animations = new ArrayList<GameAnimation>();
		winds = new ArrayList<Wind>();
		
		this.waveNo = 0;
		hud.updateMonsters(this,false);
		map.ennemyDie(this);
	}
	
	/**
	 * Lance la musique de jeu
	 */
	public void launchMusic(){
		GameConstant.MUSIC_01.play();
	}
	
	/**
	 * Etteint la musique
	 */
	public void stopMusic(){
		GameConstant.MUSIC_01.stop();
	}
	
	/**
	 * Met a jour le jeu
	 * @param launcher
	 */
	public void update(Ld38Launcher launcher){
		
		// Maj du HUD
		hud.update(this);
		map.update(this);
		
		if(!hud.isOnPause() && !hud.isOnGameOver()){
			map.update(this);
			
			Iterator<MapItem> it = items.iterator();
			while(it.hasNext()){
				MapItem item = it.next();
				if(item.isCanBeDestroy()){
					it.remove();
				}else{
					item.update(this);
				}
			}
			
			for(GameCharacter c : characters){
				c.update(this);
			}
			
			Collections.sort(characters);
			
			if(Math.random()<0.004){
				addMapItem();
			}
			
			Iterator<GameAnimation> itAnim = animations.iterator();
			while(itAnim.hasNext()){
				GameAnimation ga = itAnim.next();
				if(ga.isCanBeDestroy()){
					itAnim.remove();
				}else{
					ga.update(this);
				}
			}
			
			Iterator<Wind> itWind = winds.iterator();
			while(itWind.hasNext()){
				Wind w = itWind.next();
				if(w.isCanBeDestroy()){
					itWind.remove();
				}else{
					w.update(this);
				}
			}

//			if(Math.random()<=0.01){
//				this.winds.add(new Wind());
//			}
		}else if(hud.isOnGameOver()){
			cptGameOver++;
			if(cptGameOver >= (GameConstant.DEBUG?40:120)){
				launcher.goToTitleMenu();
			}
		}
		
		if(needUpdate){
			Iterator<GameCharacter> it = characters.iterator();
			int ennemyToAdd = 0;
			while(it.hasNext()){
				GameCharacter c = it.next();
				if(!c.equals(heros) && c.isDead()){
					it.remove();
					hud.updateMonsters(this,true);
					ennemyToAdd++;
				}
			}
			for(int i=0;i<ennemyToAdd;i++){
				map.ennemyDie(this);
			}
			needUpdate = false;
			if((characters.size())<=1){
				map.ennemyDie(this);
			}
		}
		
		
	}
	
	/**
	 * Dessine le jeu
	 * @param launcher
	 */
	public void render(Ld38Launcher launcher){
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// Dessin du contexte jeu
		GameShared.instance.gameBatch.begin();

		map.renderBackground(this);
		map.render(this);
		
		
		
		// Ici on dessine les persos qui tombent derri�re
		for(GameCharacter c : characters){
			if(c.isFallOnBack()){
				c.render(this);
			}
		}
		map.render(this);
		
		// dessine les objets
		for(MapItem it : items){
			it.render(this);
		}
		
		// Ici on dessine les persos qui sont sur la carte ou qui tombent devant
		for(GameCharacter c : characters){
			if(!c.isFallOnBack()){
				c.render(this);
			}
		}
		
		// dessine les objets
		for(GameAnimation ga : animations){
			ga.render(this);
		}
		
		// dessine les objets
		for(Wind w : winds){
			w.render(this);
		}
		
		map.renderFront(this);
		
		heros.renderMessage(this);
		
		GameShared.instance.gameBatch.end();
		
		if(GameConstant.DEBUG){
			GameShared.instance.shapeRenderer.setProjectionMatrix(GameShared.instance.gameBatch.getProjectionMatrix());
			GameShared.instance.shapeRenderer.begin();
			map.renderShape(this);
			for(GameCharacter c : characters){
				c.renderShape(this);
			}
			GameShared.instance.shapeRenderer.end();
		}
		
		// Dessin du HUD
		GameShared.instance.menuBatch.begin();
		hud.render(this);
		GameShared.instance.menuBatch.end();
	}
	
	public void addMapItem(){
		if(this.items.size()<2 && characters.size()>1){
			GamePoint p = map.getRandomTargetPoint();
			double random = Math.random();
			if(random<0.4){
				this.items.add(new MapItem(MapItem.WEAPON_SHOVEL,p.getX(),p.getY()));
			}else if(random<0.65){
				this.items.add(new MapItem(MapItem.WEAPON_HAMMER,p.getX(),p.getY()));
			}else if(random<0.85){
				this.items.add(new MapItem(MapItem.ITEM_FEATHER,p.getX(),p.getY()));
			}else{
				this.items.add(new MapItem(MapItem.ITEM_IRON_POTION,p.getX(),p.getY()));
			}
		}
	}
	
	/**
	 * 
	 * @param needUpdate
	 */
	public void setNeedUpdate(boolean needUpdate) {
		this.needUpdate = needUpdate;
	}

	/**
	 * 
	 * @return
	 */
	public Heros getHeros(){
		return this.heros;
	}
	
	/**
	 * Return la carte
	 * @return
	 */
	public GameMap getMap(){
		return map;
	}

	/**
	 * 
	 * @return
	 */
	public List<GameCharacter> getCharacters() {
		return characters;
	}
	
	public GameHud getHud(){
		return hud;
	}
	
	/**
	 * Initialise les waves d'ennemis
	 * @param no
	 */
	public void initWave(int no){
		if(GameShared.instance.levelNumber == 1){
			if(no == 0){
				this.characters.add(new Slime(440,180, Slime.BASIC_SLIME));
			}else if(no  == 1){
				this.characters.add(new Slime(150,120, Slime.BASIC_SLIME));
				this.characters.add(new Slime(170,210, Slime.SLOW_SLIME));
				this.characters.add(new Slime(480,140, Slime.SLOW_SLIME));
			}else if(no == 2){
				this.characters.add(new Slime(150,120, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(170,210, Slime.BASIC_SLIME));
				this.characters.add(new Slime(480,140, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(280,110, Slime.BASIC_SLIME));
			}else if(no == 3){
				this.characters.add(new Slime(150,120, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(170,210, Slime.BASIC_SLIME));
				this.characters.add(new Slime(480,180, Slime.BIG_SLIME));
			}else if(no == 4){
				this.characters.add(new Slime(150,120, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(170,210, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(480,140, Slime.BIG_SLIME));
				this.characters.add(new Slime(280,110, Slime.BIG_SLIME));
			}else if(no == 5){
				this.characters.add(new Slime(150,120, Slime.BIG_SLIME));
				this.characters.add(new Slime(170,210, Slime.BIG_SLIME));
				this.characters.add(new Slime(480,140, Slime.BIG_SLIME));
				this.characters.add(new Slime(280,110, Slime.BIG_SLIME));
			}else if(no == 6){
				this.characters.add(new Slime(150,120, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(170,210, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(480,140, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(280,110, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(170,120, Slime.BIG_SLIME));
				this.characters.add(new Slime(150,210, Slime.BIG_SLIME));
				this.characters.add(new Slime(430,140, Slime.BIG_SLIME));
				this.characters.add(new Slime(3100,110, Slime.BIG_SLIME));
			}else if(no == 7){
				this.characters.add(new Slime(150,120, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(170,210, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(480,140, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(280,110, Slime.AGGRO_SLIME));
				this.characters.add(new Slime(170,120, Slime.BIG_SLIME));
				this.characters.add(new Slime(150,210, Slime.BIG_SLIME));
				this.characters.add(new Slime(430,140, Slime.BIG_SLIME));
				this.characters.add(new Slime(350,180, Slime.BASIC_SLIME));
				this.characters.add(new Slime(380,180, Slime.BASIC_SLIME));
				this.characters.add(new Slime(410,180, Slime.BASIC_SLIME));
				this.characters.add(new Slime(270,180, Slime.BASIC_SLIME));
			}else{
				hud.gameOver(this);
			}
		}else if(GameShared.instance.levelNumber == 2){
			this.characters.add(new LavaRock(440,180, LavaRock.BASIC_LAVAROCK));
			this.characters.add(new LavaRock(170,210, LavaRock.BASIC_LAVAROCK));
			this.characters.add(new LavaRock(480,140, LavaRock.BIG_LAVAROCK));
			this.characters.add(new LavaRock(280,110, LavaRock.BASIC_LAVAROCK));
		}
		
		hud.updateMonsters(this, true);
	}

	/**
	 * Ajoute une animation
	 * @param gameAnimation
	 */
	public void addAnimation(GameAnimation gameAnimation) {
		this.animations.add(gameAnimation);
	}
	
	////////////////////////////////////////////////////////////////////////////////////
	// GETTERS & SETTERS                                                              //
	////////////////////////////////////////////////////////////////////////////////////
	
	public List<MapItem> getMapItems(){
		return items;
	}
	
	public int getWaveNo(){
		return waveNo;
	}
	
}
