package com.jln.ld.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.jln.ld.GameConstant;
import com.jln.ld.GameShared;
import com.jln.ld.Ld38Launcher;

/**
 * Dessine l'ecran titre
 * @author jeremy
 *
 */
public class TitleMenu {

	////////////////////////////////////////////////////////////////////////////////////
	// ATTRIBUTS                                                                      //
	////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Position du curseur sur le mode
	 * 0 = STORY
	 * 1 = ARCADE
	 * 2 = SCORE
	 * 3 = QUIT
	 */
	private int mode = 0;
	private boolean[] locked = {true,false,true,false};
	
	////////////////////////////////////////////////////////////////////////////////////
	// METHODES                                                                       //
	////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Constructeur
	 */
	public TitleMenu(){
		this.mode = 0;
	}
	
	/**
	 * Met a jour le menu
	 * @param launcher
	 */
	public void update(Ld38Launcher launcher){
		if(Gdx.input.isKeyJustPressed(Keys.DOWN)){
			if(mode<3){
				GameConstant.SOUND_BIP01.play();
				mode++;
			}
		}
		
		if(Gdx.input.isKeyJustPressed(Keys.UP)){
			if(mode>0){
				GameConstant.SOUND_BIP01.play();
				mode--;
			}
		}
		
		if(Gdx.input.isKeyJustPressed(Keys.SPACE) || Gdx.input.isKeyJustPressed(Keys.ENTER)){
			if(mode == 1){
				launcher.goToPlayerSelectionMode();
				GameConstant.SOUND_SELEC01.play();
			}else if(mode == 3){
				Gdx.app.exit();
			}else{
				// LOCKED
			}
		}
	}
	
	/**
	 * Dessine le menu
	 * @param launcher
	 */
	public void render(Ld38Launcher launcher){
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		GameShared.instance.menuBatch.begin();
		
		GameShared.instance.menuBatch.draw(GameConstant.TITLE_BACKGROUND, 0, 0);
		
		GameShared.instance.menuBatch.draw(GameConstant.TITLE_SELECTION, 498, 96-(mode*24));
		for(int i=0;i<4;i++){
			GameShared.instance.menuBatch.draw(locked[i]?GameConstant.TITLE_FRAMES[2][i]:mode==i?GameConstant.TITLE_FRAMES[1][i]:GameConstant.TITLE_FRAMES[0][i], 498, 96-(i*24));
		}
		
		GameShared.instance.font.setColor(1f, 1f, 1f, 1f);
		GameShared.instance.font.draw(GameShared.instance.menuBatch, "BEST SCORE : "+Ld38Launcher.BEST_SCORE,4,52);
		if(Ld38Launcher.LAST_SCORE>0){
			GameShared.instance.font.draw(GameShared.instance.menuBatch, "LAST SCORE : "+Ld38Launcher.LAST_SCORE,4,32);
			if(Ld38Launcher.LAST_SCORE>=Ld38Launcher.BEST_SCORE){
				GameShared.instance.font.draw(GameShared.instance.menuBatch, "(NEW RECORD !)",174,32);
			}
		}
		
		GameShared.instance.menuBatch.end();
	}
	
	////////////////////////////////////////////////////////////////////////////////////
	// GETTERS & SETTERS                                                              //
	////////////////////////////////////////////////////////////////////////////////////
}
