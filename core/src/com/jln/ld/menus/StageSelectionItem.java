package com.jln.ld.menus;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jln.ld.GameConstant;
import com.jln.ld.GameShared;
import com.jln.ld.Ld38Launcher;

public class StageSelectionItem {

public static enum STAGE {PARADISE_MOUNTAIN, PARADISE_ISLAND, VOLCANO, ICELAND, STAGE5};
	
	////////////////////////////////////////////////////////////////////////////////////
	// ATTRIBUTS                                                                      //
	////////////////////////////////////////////////////////////////////////////////////
	
	private String stageName;
	private String bestScoreValue;
	private TextureRegion frame;
	private boolean locked = true;
	private STAGE stage;
	private int caseX;
	private int frameIndexX;
	private int levelNumber;
	private GameScore bestScore;
	
	private static final int DECAL_X = 192;
	private static final int DECAL_Y = 88;
	
	////////////////////////////////////////////////////////////////////////////////////
	// METHODES                                                                       //
	////////////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * 
	 * @param stage
	 * @param locked
	 * @param caseX
	 */
	public StageSelectionItem(STAGE stage, boolean locked, int caseX){
		this.stage = stage;
		this.locked = locked;
		this.caseX = caseX;
		
		if(stage == STAGE.PARADISE_MOUNTAIN){
			this.stageName = "Pardise mountain";
			frameIndexX = 0;
		}else if(stage == STAGE.VOLCANO){
				this.stageName = "Volcano";
				frameIndexX = 1;
		}else if(stage == STAGE.PARADISE_ISLAND){
			this.stageName = "Paradise island";
			frameIndexX = 2;
		}else if(stage == STAGE.ICELAND){
			this.stageName = "Iceland";
			frameIndexX = 3;
		}else if(stage == STAGE.STAGE5){
			this.stageName = "TODO";
			frameIndexX = 4;
		}
		
		bestScoreValue="BEST : ";
		bestScore =  GameShared.instance.save.getArcadeScores()[caseX];
		if(bestScore!=null && bestScore.getScore()>0){
			bestScoreValue+= bestScore.getStringScore()+" ("+bestScore.getCharacterName()+")";
		}else{
			bestScoreValue+= "NONE";
		}
		this.levelNumber = frameIndexX +1;
		if(locked){
			this.stageName = "???";
		}
		frame = GameConstant.SELEC_STAGE_FRAMES[locked?1:0][frameIndexX];
	}
	
	/**
	 * Dessine la case de personnage
	 * @param game
	 * @param selec
	 */
	public void render(Ld38Launcher game, boolean selec, int selectedIndex){
		GameShared.instance.menuBatch.draw(frame, (caseX*260)+DECAL_X - (selectedIndex*260),DECAL_Y);
		
		if(!locked){
			GameShared.instance.menuBatch.draw(GameConstant.SELEC_STAGE_BEST_SCORE_OVER,(caseX*260)+DECAL_X - (selectedIndex*260),DECAL_Y);
			GameShared.instance.smallFont.setColor(0.1f, 0.1f, 0.1f, 1);
			GameShared.instance.smallFont.draw(GameShared.instance.menuBatch, bestScoreValue,(caseX*260)+DECAL_X - (selectedIndex*260)+7,DECAL_Y+14);
			GameShared.instance.smallFont.setColor(1f, 1f, 1f, 1);
			GameShared.instance.smallFont.draw(GameShared.instance.menuBatch, bestScoreValue,(caseX*260)+DECAL_X - (selectedIndex*260)+6,DECAL_Y+15);
		}
		
		GameShared.instance.menuBatch.draw(GameConstant.SELEC_STAGE_SELEC_FRAMES[selec?1:0][locked?1:0], (caseX*260)+DECAL_X  - (selectedIndex*260),DECAL_Y);

		if(selec){
			GameShared.instance.font.draw(GameShared.instance.menuBatch, "STAGE : "+stageName, 6, 26);
		}
	}
	
	/**
	 * Met a jour la case de personnage
	 * @param game
	 * @param selec
	 */
	public void update(Ld38Launcher game, boolean selec){
		
	}
	
	////////////////////////////////////////////////////////////////////////////////////
	// GETTERS & SETTERS                                                              //
	////////////////////////////////////////////////////////////////////////////////////

	public int getCaseX() {
		return caseX;
	}
	public int getLevelNumber() {
		return levelNumber;
	}

	public boolean isLocked() {
		return locked;
	}
}
