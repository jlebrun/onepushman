package com.jln.ld.menus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.jln.ld.GameConstant;
import com.jln.ld.GameShared;

public class GameSave {

	private GameScore[] arcadeScores;

	public GameSave(){
		arcadeScores = new GameScore[5];
	}

	/**
	 * Enregistre le fichier de sauvegarde
	 */
	public void save(){
		Gdx.files.local("onepushman.sav").delete();
		FileHandle f = Gdx.files.local("onepushman.sav");
		StringBuilder fileText = new StringBuilder();
		for(int i=0;i<5;i++){
			if(arcadeScores[i]==null){
				arcadeScores[i] = new GameScore(0,"---","---");			
			}
			fileText.append(arcadeScores[i].getFormattedScore());
			fileText.append("AAA");
		}
		if(GameConstant.DEBUG){
			System.out.println("Sauvegarde : "+fileText.toString());
			System.out.println("Possible? : "+Gdx.files.isLocalStorageAvailable());
			System.out.println("Root? : "+Gdx.files.getLocalStoragePath());
		}
		f.writeString(fileText.toString(), false);
		
	}

	/**
	 * Charge le fichier de sauvegarde
	 */
	public void load(){
		arcadeScores = new GameScore[5];
		FileHandle f = Gdx.files.local("onepushman.sav");
		boolean initFromNull = false;

		try{
			if(f.exists()){
				String file = f.readString();
				String[] lines = file.split("AAA");
				if(lines!=null && lines.length>0){
					int index = 0;
					for(String line : lines){
						if(index<5){
							String[] cases = line.split(";");
							arcadeScores[index] = new GameScore(Integer.parseInt(cases[0]),cases[1],cases[2]);
						}
						index++;
					}
				}
			}
		}catch(Exception e){
			System.out.println(e.toString());
			if(GameConstant.DEBUG){
				System.out.println("FICHER de sauvegarde non trouv�");
			}
			initFromNull = true;
		}


		if(initFromNull || !f.exists()){
			for(int i=0;i<5;i++){
				arcadeScores[i] = new GameScore(0,"---","---");
			}
			this.save();
		}
	}

	public void saveArcadeScore(int score, int mapIndex){
		if(arcadeScores[mapIndex] == null || arcadeScores[mapIndex].getScore()<score){
			Date actuelle = new Date();
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			String dat = dateFormat.format(actuelle);
			arcadeScores[mapIndex] = new GameScore(score, GameShared.instance.player1Character.getPlayerName(), dat);
			this.save();
		}
	}

	public GameScore[] getArcadeScores() {
		return arcadeScores;
	}



}
