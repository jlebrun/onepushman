package com.jln.ld.menus;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.jln.ld.GameConstant;
import com.jln.ld.GameShared;
import com.jln.ld.Ld38Launcher;

/**
 * Dessine l'ecran titre
 * @author jeremy
 *
 */
public class PlayerSelectionMenu {

	////////////////////////////////////////////////////////////////////////////////////
	// ATTRIBUTS                                                                      //
	////////////////////////////////////////////////////////////////////////////////////
	private List<PlayerSelectionItem> characters;
	private int indexX=0, indexY=0;
	private boolean playerSelected = false;

	////////////////////////////////////////////////////////////////////////////////////
	// METHODES                                                                       //
	////////////////////////////////////////////////////////////////////////////////////

	/** 
	 * Constructeur
	 */
	public PlayerSelectionMenu(){
	}

	/**
	 * Methode d'initialisation
	 */
	public void init(){
		this.playerSelected = false;
		this.indexX = 0;
		this.indexY = 0;
		
		characters = new ArrayList<PlayerSelectionItem>();
		characters.add(new PlayerSelectionItem(PlayerSelectionItem.CHARACTER.GREENBOY, false, 0, 0));
		characters.add(new PlayerSelectionItem(PlayerSelectionItem.CHARACTER.BLACKSOLDIER, false, 1, 0));
		characters.add(new PlayerSelectionItem(PlayerSelectionItem.CHARACTER.WIZARD, false, 2, 0));
		characters.add(new PlayerSelectionItem(PlayerSelectionItem.CHARACTER.HARUSAME, false, 3, 0));
		characters.add(new PlayerSelectionItem(PlayerSelectionItem.CHARACTER.YAMI, true, 4, 0));

		characters.add(new PlayerSelectionItem(PlayerSelectionItem.CHARACTER.CAULFIELD, false, 0, 1));
		characters.add(new PlayerSelectionItem(PlayerSelectionItem.CHARACTER.FLANE, false, 1, 1));
		characters.add(new PlayerSelectionItem(PlayerSelectionItem.CHARACTER.RELM, true, 2, 1));
		characters.add(new PlayerSelectionItem(PlayerSelectionItem.CHARACTER.ALEXRE, true, 3, 1));
		characters.add(new PlayerSelectionItem(PlayerSelectionItem.CHARACTER.GARYWISS, false, 4, 1));
	}

	/**
	 * Met a jour le menu
	 * @param launcher
	 */
	public void update(Ld38Launcher launcher){
		if(!playerSelected){
			if(Gdx.input.isKeyJustPressed(Keys.DOWN)){
				if(indexY<1){
					GameConstant.SOUND_BIP01.play();
					indexY++;
				}
			}

			if(Gdx.input.isKeyJustPressed(Keys.UP)){
				if(indexY>0){
					GameConstant.SOUND_BIP01.play();
					indexY--;
				}
			}
			if(Gdx.input.isKeyJustPressed(Keys.RIGHT)){
				if(indexX<4){
					GameConstant.SOUND_BIP01.play();
					indexX++;
				}
			}

			if(Gdx.input.isKeyJustPressed(Keys.LEFT)){
				if(indexX>0){
					GameConstant.SOUND_BIP01.play();
					indexX--;
				}
			}
		}

		if(Gdx.input.isKeyJustPressed(Keys.ESCAPE)){
			if(playerSelected){
				GameConstant.SOUND_SELEC01.play();
				playerSelected = false;
			}else{
				GameConstant.SOUND_SELEC01.play();
				launcher.goToTitleMenu();
			}
		}

		if(Gdx.input.isKeyJustPressed(Keys.SPACE) || Gdx.input.isKeyJustPressed(Keys.ENTER)){
			if(!playerSelected){
				PlayerSelectionItem player = characters.get((indexY*5)+indexX);
				if(!player.isLocked()){
					GameConstant.SOUND_SELEC01.play();
					this.playerSelected = true;
					GameShared.instance.player1Character = player;
				}else{
					GameConstant.SOUND_FALL.play();
				}
			}else{
				launcher.goToStageSelectionMode();
				GameConstant.SOUND_SELEC01.play();
			}
		}
	}

	/**
	 * Dessine le menu
	 * @param launcher
	 */
	public void render(Ld38Launcher launcher){
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		GameShared.instance.menuBatch.begin();

		GameShared.instance.menuBatch.draw(GameConstant.SELEC_CHARACTERS_BACKGROUND, 0, 0);
		GameShared.instance.menuBatch.draw(GameConstant.SELEC_CHARACTERS_MENU, 0, 0);

		GameShared.instance.menuBatch.draw(GameConstant.SELEC_CHARACTERS_PLAYER_BOX, 16, 0);

		for(PlayerSelectionItem chara : characters){
			chara.render(launcher, (indexX==chara.getCaseX() && indexY==chara.getCaseY()));
		}

		GameShared.instance.menuBatch.draw(playerSelected?GameConstant.SELEC_VALID_SELEC:GameConstant.SELEC_VALID, 488, 0);

		GameShared.instance.menuBatch.end();
	}

	////////////////////////////////////////////////////////////////////////////////////
	// GETTERS & SETTERS                                                              //
	////////////////////////////////////////////////////////////////////////////////////
}
