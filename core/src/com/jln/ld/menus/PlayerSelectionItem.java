package com.jln.ld.menus;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jln.ld.GameConstant;
import com.jln.ld.GameShared;
import com.jln.ld.Ld38Launcher;

public class PlayerSelectionItem {

	public static enum CHARACTER {GREENBOY, BLACKSOLDIER,WIZARD, HARUSAME, YAMI, CAULFIELD, FLANE, RELM, ALEXRE, GARYWISS};
	
	////////////////////////////////////////////////////////////////////////////////////
	// ATTRIBUTS                                                                      //
	////////////////////////////////////////////////////////////////////////////////////
	
	private String playerName;
	private String punchline;
	private TextureRegion frame, selectedFrame;
	private boolean locked = true;
	private CHARACTER character;
	private int caseX;
	private int caseY;
	private int strength; // 1 to 16
	private int speed; // 1 to 16;
	
	private static final int DECAL_X = 76;
	private static final int DECAL_Y = 180;
	
	////////////////////////////////////////////////////////////////////////////////////
	// METHODES                                                                       //
	////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Constructeur
	 * @param character
	 * @param locked
	 * @param caseX
	 * @param caseY
	 */
	public PlayerSelectionItem(CHARACTER character, boolean locked, int caseX, int caseY){
		this.character = character;
		this.locked = locked;
		this.caseX = caseX;
		this.caseY = caseY;
		
		int frameIndexX = 0;
		if(character == CHARACTER.GREENBOY){
			frameIndexX = 0;
			playerName = "Green boy";
			punchline = "TODO";
			strength = 8;
			speed = 9;
		}else if(character == CHARACTER.BLACKSOLDIER){
			frameIndexX = 1;
			playerName = "Soldier";
			punchline = "TODO";
			strength = 15;
			speed = 5;
		}else if(character == CHARACTER.WIZARD){
			frameIndexX = 2;
			playerName = "Wizard";
			punchline = "TODO";
			strength = 12;
			speed = 8;
		}else if(character == CHARACTER.HARUSAME){
			frameIndexX = 3;
			playerName = "Haru";
			punchline = "Close the World, Open the next.";
			strength = 12;
			speed = 10;
		}else if(character == CHARACTER.YAMI){
			frameIndexX = 4;
			playerName = "Yami";
			punchline = "In darkness, we trust !";
			strength = 10;
			speed = 10;
		}else if(character == CHARACTER.CAULFIELD){
			frameIndexX = 5;
			playerName = "Caulfield";
			punchline = "Who I am ?";
			strength = 10;
			speed = 14;
		}else if(character == CHARACTER.FLANE){
			frameIndexX = 6;
			playerName = "Flane";
			punchline = "TODO";
			strength = 16;
			speed = 6;
		}else if(character == CHARACTER.RELM){
			frameIndexX = 7;
			playerName = "Relm";
			punchline = "TODO";
			strength = 8;
			speed = 12;
		}else if(character == CHARACTER.ALEXRE){
			frameIndexX = 8;
			playerName = "Alex RE";
			punchline = "TODO";
			strength = 8;
			speed = 8;
		}else if(character == CHARACTER.GARYWISS){
			frameIndexX = 9;
			playerName = "Garywiss";
			punchline = "TODO";
			strength = 6;
			speed = 14;
		}
		
		frame = GameConstant.SELEC_CHARACTERS_PLAYER_FRAMES[locked?1:0][frameIndexX];
		if(locked){
			playerName = "???";
			punchline = "???";
			selectedFrame = new TextureRegion(GameConstant.SELEC_CHARACTERS_LOCKED_ICON);
		}else{
			selectedFrame = GameConstant.SELEC_CHARACTERS_PLAYER_FRAMES[2][frameIndexX];
		}
	}
	
	/**
	 * Dessine la case de personnage
	 * @param game
	 * @param selec
	 */
	public void render(Ld38Launcher game, boolean selec){
		GameShared.instance.menuBatch.draw(frame, (caseX*100)+DECAL_X,DECAL_Y-(caseY*100));
		GameShared.instance.menuBatch.draw(GameConstant.SELEC_CHARACTERS_SELEC_FRAMES[locked?1:0][selec?1:0], (caseX*100)+DECAL_X,DECAL_Y-(caseY*100));
		
		if(selec){
			GameShared.instance.menuBatch.draw(selectedFrame,-11,-12);
			GameShared.instance.font.setColor(0.1f, 0.1f, 0.1f, 1f);
			GameShared.instance.font.draw(GameShared.instance.menuBatch, playerName, 61, 58);
			GameShared.instance.font.setColor(1f, 1f, 1f, 1f);
			GameShared.instance.font.draw(GameShared.instance.menuBatch, playerName, 60, 59);
			
			this.renderStars(107,23,locked?0:strength);
			this.renderStars(107,3,locked?0:speed);
		}
	}
	
	/**
	 * Dessine les etoiles 
	 * @param xOrigine
	 * @param yOrigine
	 * @param value
	 */
	private void renderStars(int xOrigine, int yOrigine, int value){
		int nbStar = 0;
		while(nbStar<5){
			nbStar++;
			if(value>=4){
				GameShared.instance.menuBatch.draw(GameConstant.SELEC_CHARACTERS_STAR[0][0],xOrigine+(nbStar*16),yOrigine);
				value-=4;
			}else if(value==3){
				GameShared.instance.menuBatch.draw(GameConstant.SELEC_CHARACTERS_STAR[0][1],xOrigine+(nbStar*16),yOrigine);
				value-=3;
			}else if(value==2){
				GameShared.instance.menuBatch.draw(GameConstant.SELEC_CHARACTERS_STAR[0][2],xOrigine+(nbStar*16),yOrigine);
				value-=2;
			}else if(value==1){
				GameShared.instance.menuBatch.draw(GameConstant.SELEC_CHARACTERS_STAR[0][3],xOrigine+(nbStar*16),yOrigine);
				value-=1;
			}else{
				GameShared.instance.menuBatch.draw(GameConstant.SELEC_CHARACTERS_STAR[0][4],xOrigine+(nbStar*16),yOrigine);
			}
		}
		
		
	}
	
	/**
	 * Met a jour la case de personnage
	 * @param game
	 * @param selec
	 */
	public void update(Ld38Launcher game, boolean selec){
		
	}
	
	////////////////////////////////////////////////////////////////////////////////////
	// GETTERS & SETTERS                                                              //
	////////////////////////////////////////////////////////////////////////////////////

	public int getCaseX() {
		return caseX;
	}

	public int getCaseY() {
		return caseY;
	}

	public boolean isLocked() {
		return locked;
	}

	public CHARACTER getCharacter() {
		return character;
	}

	public String getPlayerName() {
		return playerName;
	}
	
	public float getFloatSpeed(){
		return (float)(speed/10f)+0.4f;
	}
	
	public float getFloatStrength(){
		return (float)(((strength*1f)/3f)+2f);
	}
}
