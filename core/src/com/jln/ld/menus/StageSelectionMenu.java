package com.jln.ld.menus;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.jln.ld.GameConstant;
import com.jln.ld.GameShared;
import com.jln.ld.Ld38Launcher;

/**
 * Dessine l'ecran titre
 * @author jeremy
 *
 */
public class StageSelectionMenu {
	
	////////////////////////////////////////////////////////////////////////////////////
	// ATTRIBUTS                                                                      //
	////////////////////////////////////////////////////////////////////////////////////
	private List<StageSelectionItem> stages;
	private int indexX=0;
	private boolean stageSelected = false;
	
	////////////////////////////////////////////////////////////////////////////////////
	// METHODES                                                                       //
	////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Constructeur
	 */
	public StageSelectionMenu(){
		
	}
	
	/**
	 * Methode d'initialisation
	 */
	public void init(){
		this.stageSelected = false;
		this.indexX = 0;
		
		stages = new ArrayList<StageSelectionItem>();
		stages.add(new StageSelectionItem(StageSelectionItem.STAGE.PARADISE_MOUNTAIN, false, 0));
		stages.add(new StageSelectionItem(StageSelectionItem.STAGE.VOLCANO, false, 1));
		stages.add(new StageSelectionItem(StageSelectionItem.STAGE.PARADISE_ISLAND, true, 2));
		stages.add(new StageSelectionItem(StageSelectionItem.STAGE.ICELAND, true, 3));
		stages.add(new StageSelectionItem(StageSelectionItem.STAGE.STAGE5, true, 4));
	}
	
	/**
	 * Met a jour le menu
	 * @param launcher
	 */
	public void update(Ld38Launcher launcher){
		if(Gdx.input.isKeyJustPressed(Keys.RIGHT)){
			if(indexX<4){
				GameConstant.SOUND_BIP01.play();
				indexX++;
			}
		}
		
		if(Gdx.input.isKeyJustPressed(Keys.LEFT)){
			if(indexX>0){
				GameConstant.SOUND_BIP01.play();
				indexX--;
			}
		}
		
		if(Gdx.input.isKeyJustPressed(Keys.SPACE) || Gdx.input.isKeyJustPressed(Keys.ENTER)){
			if(!stageSelected){
				StageSelectionItem stage = stages.get(indexX);
				if(!stage.isLocked()){
					GameConstant.SOUND_SELEC01.play();
					this.stageSelected = true;
					GameShared.instance.levelNumber = stage.getLevelNumber();
				}else{
					GameConstant.SOUND_FALL.play();
				}
			}else{
				launcher.goToGameMode();
				GameConstant.SOUND_SELEC01.play();
			}
		}
		
		if(Gdx.input.isKeyJustPressed(Keys.ESCAPE)){
			if(stageSelected){
				GameConstant.SOUND_SELEC01.play();
				stageSelected = false;
			}else{
				GameConstant.SOUND_SELEC01.play();
				launcher.goToPlayerSelectionMode();
			}
		}
	}
	
	/**
	 * Dessine le menu
	 * @param launcher
	 */
	public void render(Ld38Launcher launcher){
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		GameShared.instance.menuBatch.begin();
		
		GameShared.instance.menuBatch.draw(GameConstant.SELEC_STAGE_BACKGROUND, 0, 0);
		GameShared.instance.menuBatch.draw(GameConstant.SELEC_STAGE_MENU, 0, 0);
		
		for(StageSelectionItem stage : stages){
			stage.render(launcher, indexX==stage.getCaseX(), indexX);
		}
		
		GameShared.instance.menuBatch.draw(stageSelected?GameConstant.SELEC_VALID_SELEC:GameConstant.SELEC_VALID, 488, 0);
		
		GameShared.instance.menuBatch.end();
	}
	
	////////////////////////////////////////////////////////////////////////////////////
	// GETTERS & SETTERS                                                              //
	////////////////////////////////////////////////////////////////////////////////////
}
