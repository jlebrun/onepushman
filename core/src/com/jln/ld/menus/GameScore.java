package com.jln.ld.menus;

public class GameScore {

	private int score = 0;
	private String characterName="";
	private String date="";
	private String stringScore;
	
	public GameScore(int score, String characterName, String date){
		this.score = score;
		this.characterName = characterName;
		this.date = date;
		stringScore = ""+score;
		while(stringScore.length()<4){
			stringScore = "0"+stringScore;
		}
	}
	

	public int getScore() {
		return score;
	}

	public String getStringScore() {
		return stringScore;
	}
	
	public String getCharacterName() {
		return characterName;
	}

	public String getDate() {
		return date;
	}
	
	public String getFormattedScore(){
		return score+";"+characterName+";"+date;
	}
	
}
