package com.jln.ld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.jln.ld.menus.GameMenu;

public class GameAnimation extends GameEntity{
	
	private boolean canBeDestroy = false;
	private float x,y;
	private Animation animation;
	private float stateTime = 0f;
	private int type;
	
	public static final int ANIM_HIT_SIMPLE = 0;
	public static final int ANIM_HIT_SHOVEL = 1;
	public static final int ANIM_HIT_DOUBLE_WEAPONS = 2;
	
	public static final int ANIM_FEATHER = 3;
	public static final int ANIM_IRON_POTION = 4;
	
	private int cellSize = 16;
	private int coldown;
	
	/**
	 * Constructeur
	 * @param type
	 * @param x
	 * @param y
	 */
	public GameAnimation(int type, float x, float y){
		this.x = x;
		this.y = y;
		this.type = type;
		if(type == ANIM_HIT_SIMPLE){
			animation = GameUtils.loadAnimation(GameConstant.WEAPON_HIT, 4, (int)(Math.random()*4), 0.09f);
			cellSize = 16;
		}else if(type == ANIM_HIT_SHOVEL){
			animation = GameUtils.loadAnimation(GameConstant.WEAPON_HIT, 4, (int)(Math.random()*4)+4, 0.09f);
			cellSize = 16;
		}else if(type == ANIM_HIT_DOUBLE_WEAPONS){
			animation = GameUtils.loadAnimation(GameConstant.WEAPON_HIT, 4, (int)(Math.random()*2)+8, 0.09f);
			cellSize = 16;
		}else if(type == ANIM_FEATHER){
			animation = GameUtils.loadAnimation(GameConstant.ANIM_FEATHER, 4, (int)(Math.random()*4), 0.09f);
			cellSize = 16;
		}else if(type == ANIM_IRON_POTION){
			animation = GameUtils.loadAnimation(GameConstant.ANIM_IRON_POTION, 4, (int)(Math.random()*4), 0.09f);
			cellSize = 16;
		}
		animation.setPlayMode(PlayMode.NORMAL);
		coldown = 0;
	}
	
	/**
	 * Constructeur
	 * @param animation
	 * @param x
	 * @param y
	 */
	public GameAnimation(Animation theAnimation, int cellSize, float x, float y, int coldown){
		this.x = x;
		this.y = y;
		
		this.cellSize = cellSize;
		this.coldown = coldown;
		
		this.animation = theAnimation;
		this.animation.setPlayMode(PlayMode.NORMAL);
		stateTime =0f; 
	}
	
	/**
	 * Met a jour l'animation
	 */
	public void update(GameMenu game){
		if(coldown>0){
			coldown--;
		}else{
			if(animation.isAnimationFinished(stateTime)){
				canBeDestroy = true;
			}else{
				stateTime += Gdx.graphics.getDeltaTime(); 
			}
		}
		
	}
	
	/**
	 * Dessine l'animation
	 */
	public void render(GameMenu game){
		if(coldown>0){
			coldown--;
		}else{
			if(animation != null){
				GameShared.instance.gameBatch.draw(animation.getKeyFrame(stateTime), (int)(x-(cellSize/2)),(int)(y-(cellSize/2)));
			}
		}
		
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isCanBeDestroy(){
		return canBeDestroy;
	}
}
