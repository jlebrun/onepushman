package com.jln.ld.characters;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.math.Rectangle;
import com.jln.ld.GameAnimation;
import com.jln.ld.GameConstant;
import com.jln.ld.GameEntity;
import com.jln.ld.GameShared;
import com.jln.ld.GameUtils;
import com.jln.ld.map.MapItem;
import com.jln.ld.menus.GameMenu;

public class GameCharacter extends GameEntity implements Comparable<GameCharacter>{

	protected float x=(GameConstant.GAME_WIDTH/2);
	protected float y=(GameConstant.GAME_HEIGHT/2);
	protected boolean fall;
	protected float fallAcceleration = 1;
	protected int direction = GameConstant.DIR_DOWN;

	protected float stateTime = 0f;
	protected float stateTimeMove = 0f;
	protected boolean fallOnBack = false;
	protected int waitFall = 0;
	
	protected float colorHit = 1f;
	protected float colorGray = 1f;

	protected Rectangle collision;
	protected Rectangle collisionWeapon;
	protected int collisionWidth = 0;
	protected int collisionHeight = 0;


	protected float pushX = 0;
	protected float pushY = 0;

	protected GameCharacter pusher = null;
	protected int pusherTimer;

	public static int COMPTEUR = 0;
	protected int id;
	protected int decalRenderY = 0;

	protected Sound pushSound;
	protected Sound fallSound;


	protected Animation appearAnimation;
	protected Animation appear2Animation;
	protected Animation[] itemShovelAnimation;
	protected Animation itemHammerAnimation;

	protected boolean appearPlay = false;
	protected boolean appear2Play = false;
	protected boolean dead = false;

	protected int item = MapItem.NO_ITEM;

	protected int itemTime = 0;

	protected int coldownPush = 0;

	float tampPushX = 0;
	float tampPushY = 0;
	float speedFactor = 1f;
	
	protected float strength = 5f;
	protected float speed = 1.2f;
	
	protected List<GameCharacterGhost> ghosts;

	/**
	 * Constructeur
	 */
	public GameCharacter(){
		this.id = COMPTEUR;
		COMPTEUR++;
		appearPlay = false;
		ghosts = new ArrayList<GameCharacterGhost>();
	}

	/**
	 * Initialise les ressources
	 */
	protected void initRessources(){
		TextureRegion[][] tmp = TextureRegion.split(GameConstant.TX_APPEAR, 64, 224);
		this.appearAnimation = GameUtils.loadAnimation(tmp, 5, 0, 0.08f);
		this.appear2Animation = GameUtils.loadAnimation(tmp, 5, 1, 0.08f);
		
		itemShovelAnimation = new Animation[5];
		for(int i=0;i<5;i++){
			this.itemShovelAnimation[i] = GameUtils.loadAnimation(GameConstant.WEAPON_SHOVEL, 4, i, 0.08f);
			this.itemShovelAnimation[i].setPlayMode(PlayMode.LOOP);
		}
		this.itemHammerAnimation = GameUtils.loadAnimation(GameConstant.WEAPON_HAMMER, 4, 0, 0.08f);
		this.appearAnimation.setPlayMode(PlayMode.NORMAL);
		this.appear2Animation.setPlayMode(PlayMode.NORMAL);
		
		this.itemHammerAnimation.setPlayMode(PlayMode.LOOP);

	}

	/**
	 * Maj le personnage
	 * @param game
	 */
	public void update(GameMenu game){

		if(coldownPush>0){
			coldownPush--;
		}
		if(colorHit<1f){
			colorHit+=0.05f;
			if(colorHit>1f){
				colorHit = 1f;
			}
		}
		
		if(colorGray<1f && item!=MapItem.ITEM_IRON_POTION){
			colorGray+=0.05f;
			if(colorGray>1f){
				colorGray = 1f;
			}
		}

		if(!ghosts.isEmpty()){
			Iterator<GameCharacterGhost> it = ghosts.iterator();
			GameCharacterGhost g;
			while(it.hasNext()){
				g = it.next();
				if(g.isDestroy()){
					it.remove();
				}else{
					g.update(game);
				}
			}
		}
		
		if(!dead && appearPlay){
			if(!fall){

				if(item == MapItem.ITEM_FEATHER){
					speedFactor = 2f;
				}else if(item == MapItem.WEAPON_HAMMER){
					speedFactor = 0.6f;
				}else if(item == MapItem.ITEM_IRON_POTION){
					speedFactor = 0.3f;
				}else{
					speedFactor = 1f;
				}

				if(item != MapItem.NO_ITEM && itemTime>0){
					itemTime--;
					if(itemTime<=0){
						item = MapItem.NO_ITEM;
					}
				}
				if(pusherTimer>0){
					pusherTimer--;
					if(pusherTimer<=0){
						pusher = null;
					}
				}
				if(pushX == 0 && pushY == 0){
					if(game.getMap().getMapState(x, y)>=2){
						this.fall = true;
						this.direction = GameConstant.DIR_FALL;
						this.fallAcceleration = 0.5f;
						this.fallSound.play();
						if(game.getMap().getMapState(x, y)==3){
							fallOnBack = true;
						}
						if(game.getHeros().equals(pusher)){
							game.getHud().updateKills(game, getPoint(),true);
						}else{
							game.getHud().updateKills(game, 1,true);
						}
					}
				}else{
					// On met a jour la pouss�e
					this.updatePush(game);
				}

				if(item == MapItem.ITEM_FEATHER && itemTime%5==0){
					game.addAnimation(new GameAnimation(GameAnimation.ANIM_FEATHER,x,y));
				}else if(item == MapItem.ITEM_IRON_POTION && itemTime%5==0){
					game.addAnimation(new GameAnimation(GameAnimation.ANIM_IRON_POTION,x,y));
				}
			}else if(y>-68){
				if(waitFall<10){
					waitFall++;
				}else{
					if(this.fallAcceleration<5){
						this.fallAcceleration+=0.3f;
					}
					this.y-=fallAcceleration;
					this.updateCollision(game,0, -fallAcceleration);
				}
			}else{
				die(game);
			}
		}
	}

	/**
	 * Deplace le personnage
	 * @param game
	 * @param dx
	 * @param dy
	 */
	protected void move(GameMenu game, float dx, float dy){
		if(this.item == MapItem.ITEM_FEATHER){
			addGhost(game, new ArrayList<GameCharacterGhostFrame>());
		}
		this.x +=dx;
		this.y += dy;
		this.updateCollision(game,dx, dy);
	}
	
	
	/**
	 * Met a jour la pouss�e
	 * @param game
	 */
	private void updatePush(GameMenu game){
		tampPushX = 0;
		tampPushY = 0;

		if(pushX>5){
			tampPushX = pushX -5;
			pushX =5;
		}
		if(pushX<-5){
			tampPushX = pushX +5;
			pushX =-5;
		}
		if(pushY>5){
			tampPushY = pushY -5;
			pushY =5;
		}
		if(pushY<-5){
			tampPushY = pushY +5;
			pushY =-5;
		}

		int mapState = game.getMap().getMapState(x+pushX, y+pushY);
		if( mapState == 0 || mapState >= 2){
			this.x += pushX;
			this.y += pushY;
			this.updateCollision(game,pushX, pushY);
		}
		this.pushX *=0.6;
		this.pushY *=0.6;

		this.pushX+=tampPushX;
		this.pushY+=tampPushY;

		if(Math.abs(pushX)<0.2){
			this.pushX = 0;
		}
		if(Math.abs(pushY)<0.2){
			this.pushY = 0;
		}
	}

	/**
	 * 
	 * @param game
	 */
	protected void die(GameMenu game){
		this.dead = true;
		game.setNeedUpdate(true);
	}

	/**
	 * 
	 * @param game
	 * @param dx
	 * @param dy
	 * @return
	 */
	protected boolean isBlocked(GameMenu game,float xx, float yy){
		if(game.getMap().getMapState(xx, yy) ==1){
			return true;
		}

		return false;
	}

	/**
	 * Dessine le personnage
	 * @param game
	 */
	public void render(GameMenu game){
		this.renderAppear(game);

		if(!ghosts.isEmpty()){
			for (GameCharacterGhost g : ghosts) {
				g.render(game);
			}
		}
		if(item == MapItem.WEAPON_SHOVEL){ 
			GameShared.instance.gameBatch.draw(itemShovelAnimation[direction].getKeyFrame(stateTime),(int)(x-32), (int)(y-32));
		}else if(item == MapItem.WEAPON_HAMMER){
			GameShared.instance.gameBatch.draw(itemHammerAnimation.getKeyFrame(stateTime),(int)(x-32), (int)(y-32));
		}
	}
	
	/**
	 * Ajoute une frame fantome
	 * @param game
	 * @param frames
	 */
	protected void addGhost(GameMenu game, List<GameCharacterGhostFrame> frames){
		// A redefinir avant pour ajouter le ghost du personnage
		if(item == MapItem.WEAPON_SHOVEL){ 
			frames.add(new GameCharacterGhostFrame(itemShovelAnimation[direction].getKeyFrame(stateTime),(int)(x-32), (int)(y-32)));
		}else if(item == MapItem.WEAPON_HAMMER){
			frames.add(new GameCharacterGhostFrame(itemHammerAnimation.getKeyFrame(stateTime),(int)(x-32), (int)(y-32)));
		}
		ghosts.add(new GameCharacterGhost(x,y,frames));
	}


	public void renderShape(GameMenu game){
		GameShared.instance.shapeRenderer.setColor(1, 0, 0, 1);
		GameShared.instance.shapeRenderer.rect(collision.x, collision.y, collision.width, collision.height);

		GameShared.instance.shapeRenderer.setColor(1, 1, 0, 1);
		GameShared.instance.shapeRenderer.rect(collisionWeapon.x, collisionWeapon.y, collisionWeapon.width, collisionWeapon.height);

		GameShared.instance.shapeRenderer.setColor(1, 1, 1, 1);
	}

	/**
	 * Dessine l'apparition
	 * @param game
	 */
	public void renderAppear(GameMenu game){
		if(!appearPlay){
			GameShared.instance.gameBatch.draw(appearAnimation.getKeyFrame(stateTime),(int)(x-32), (int)(y-16));
			if(appearAnimation.isAnimationFinished(stateTime)){
				appearPlay = true;
				GameConstant.SOUND_APPEAR.play();
				stateTime = 0f;
			}
		}
		if(!appear2Play){
			GameShared.instance.gameBatch.draw(appear2Animation.getKeyFrame(stateTime),(int)(x-32), (int)(y-16));
			if(appear2Animation.isAnimationFinished(stateTime)){
				appear2Play = true;
			}
		}
	}

	/**
	 * Initialise le rectangle de collision
	 */
	protected void initCollision(){
		this.collision = new Rectangle(x-(collisionWidth/2), (y-(collisionHeight/2)),collisionWidth,collisionHeight);
		this.collisionWeapon = new Rectangle(x-26, y-26,52,52);
	}

	/**
	 * Met a jour le rectangle de collisions
	 * @param game
	 * @param dx
	 * @param dy
	 */
	protected void updateCollision(GameMenu game,float dx, float dy){
		collision.setX(collision.getX()+dx);
		collision.setY(collision.getY()+dy);

		float decalWeaponX = 0;
		float decalWeaponY = 0;
		if(item == MapItem.WEAPON_SHOVEL){
			if(direction==GameConstant.DIR_LEFT || direction==GameConstant.DIR_RIGHT ){
				collisionWeapon.setWidth(38);
				collisionWeapon.setHeight(24);
				if(direction==GameConstant.DIR_RIGHT){
					decalWeaponX = 14;
				}
				decalWeaponY = 14;
			}else{
				collisionWeapon.setWidth(24);
				collisionWeapon.setHeight(38);
				if(direction==GameConstant.DIR_UP){
					decalWeaponY = 14;
				}
				decalWeaponX = 14;
			}
		}else{
			collisionWeapon.setWidth(52);
			collisionWeapon.setHeight(52);
		}
		collisionWeapon.setX(x-26+dx+decalWeaponX);
		collisionWeapon.setY(y-26+dx+decalWeaponY);

		// On essaye de pousser
		for(GameCharacter c : game.getCharacters()){
			if(!c.equals(this) && c.appearPlay && c.appear2Play && c.getColdownPush()<=0){
				if(this.onContact(c.hasAWeapon()?c.getWeaponCollision():c.getCollision())){
					this.push(game,c, dx, dy);
				}
			}
		}
	}


	/**
	 * Pousse un autre personnage
	 * @param game
	 * @param c
	 */
	public void push(GameMenu game, GameCharacter c, float ddx, float ddy ){ 
		if(pusher == null || (!pusher.equals(c) && !this.equals(c.getPusher()))){
			float finalDx = 0;
			float finalDy = 0;
			boolean dontPush = false;

			if(direction == GameConstant.DIR_DOWN && c.y < y){
				finalDy = -1 + (0.5f*ddx);
			}else if(direction == GameConstant.DIR_UP && c.y > y){
				finalDy = 1 + (0.5f*ddx);
			}else if(direction == GameConstant.DIR_LEFT && c.x < x){
				finalDx =-1 + (0.5f*ddy);
			}else if(direction == GameConstant.DIR_RIGHT && c.x > x){
				finalDx =1 + (0.5f*ddy);
			}

			float pushFactor = getStrength();
			float decalFactor = 2;
			if(item == MapItem.WEAPON_SHOVEL){
				pushFactor *= 1.7f;
				decalFactor = 1f;
			}else if(item == MapItem.WEAPON_HAMMER){
				pushFactor *= 2.2f;
				decalFactor=1f;
			}else if(item == MapItem.ITEM_FEATHER){
				pushFactor += Math.abs(ddx)+Math.abs(ddy);
				decalFactor = 4f;
			}else if(item == MapItem.ITEM_IRON_POTION){
				decalFactor = 0f;
			}else{
				pushFactor = strength;
				decalFactor = 2;
			}

			if(c.getItem() == MapItem.ITEM_IRON_POTION){
				pushFactor = c.getStrength()*0.25f;
			}else if(c.getItem() == MapItem.ITEM_FEATHER){
				pushFactor = c.getStrength()*1.5f;
			}else if(c.getItem() == MapItem.WEAPON_HAMMER || c.getItem() == MapItem.WEAPON_SHOVEL){
				pushFactor = c.getStrength()*0.8f;
				if(item == MapItem.NO_ITEM || item == MapItem.ITEM_FEATHER){
					dontPush = true; 
					// c'est l'arme de l'ennemi qui pousse, pas nous
				}else{
					decalFactor = decalFactor*2f;
				}
			}

			if(!dontPush){
				if(finalDx != 0 || finalDy != 0){
					this.pushX += (decalFactor*(-finalDx));	
					this.pushY += (decalFactor*(-finalDy));	
					c.setPushX((pushFactor*finalDx));
					c.setPushY((pushFactor*finalDy));
					c.setPusher(game,this);
					pushSound.play();
					c.setColdownPush(5);
				}
				float xCenter = this.x + ((c.getX()-this.getX())/2);
				float yCenter = this.y + ((c.getY()-this.getY())/2);
				if(item == MapItem.NO_ITEM || item == MapItem.ITEM_FEATHER || item == MapItem.ITEM_IRON_POTION ){
					game.addAnimation(new GameAnimation(GameAnimation.ANIM_HIT_SIMPLE,xCenter, yCenter));
				}else if(c.getItem() == MapItem.ITEM_IRON_POTION){
					game.addAnimation(new GameAnimation(GameAnimation.ANIM_HIT_SHOVEL,xCenter, yCenter));
				}else if(c.hasAWeapon() && c.getItemTime() != MapItem.ITEM_FEATHER){
					game.addAnimation(new GameAnimation(GameAnimation.ANIM_HIT_DOUBLE_WEAPONS,xCenter, yCenter));
				}else if(item == MapItem.WEAPON_SHOVEL || item == MapItem.WEAPON_HAMMER){
					game.addAnimation(new GameAnimation(GameAnimation.ANIM_HIT_SHOVEL,xCenter, yCenter));
				}
			}
		}
	}
	
	/**
	 * Pousse un autre personnage
	 * @param game
	 * @param c
	 */
	public void pushByItem(GameMenu game, float ddx, float ddy ){ 
		float decalFactor = 1f;
		if( item == MapItem.ITEM_FEATHER){
			decalFactor = 2f;
		}else if(item == MapItem.ITEM_IRON_POTION){
			decalFactor = 0.5f;
		}
		this.pushX = (decalFactor*ddx);	
		this.pushY = (decalFactor*ddy);	
		colorHit = 0f;
		this.setColdownPush(5);
	}


	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	public boolean isFall() {
		return fall;
	}
	public float getFallAcceleration() {
		return fallAcceleration;
	}
	public boolean isFallOnBack() {
		return fallOnBack;
	}


	public boolean onContact(Rectangle r){
		if(hasAWeapon()){
			return getWeaponCollision().overlaps(r);
		}else{
			return collision.overlaps(r);
		}
	}
	
	/**
	 * Defini la couleur avant dessin du sprite
	 * @param game
	 */
	public void colorBeforeDraw(GameMenu game){
		if(colorGray<1f){
			GameShared.instance.gameBatch.setColor(colorGray, colorGray, colorGray, 1f);
		}else if(colorHit<1f){
			GameShared.instance.gameBatch.setColor(1f, colorHit, colorHit, 1f);
		}
	}
	
	/**
	 * Stoppe la couleur apr�s dessin du sprite
	 * @param game
	 */
	public void colorBackToOrigine(GameMenu game){
		if(colorHit<1f || colorGray <1f){
			GameShared.instance.gameBatch.setColor(1f, 1f, 1f, 1f);
		}

	}

	
	public boolean onCoreContact(Rectangle r){
		return collision.overlaps(r);
	}

	/**
	 * Retourne la collision sans arme
	 * @return
	 */
	public Rectangle getCollision() {
		return collision;
	}

	/**
	 * Retourne la collision avec une arme 
	 * @return
	 */
	public Rectangle getWeaponCollision() {
		if(item == MapItem.ITEM_FEATHER || item == MapItem.ITEM_IRON_POTION){
			return collision; 
		}
		return collisionWeapon;
	}
	public float getPushX() {
		return pushX;
	}
	public void setPushX(float pushX) {
		this.pushX = pushX;
	}
	public float getPushY() {
		return pushY;
	}
	public void setPushY(float pushY) {
		this.pushY = pushY;
	}
	public void setPusher(GameMenu game,GameCharacter pusher) {
		this.pusher = pusher;
		this.pusherTimer = 20;
		if(colorHit>0.5f){
			this.colorHit = 0.5f;
		}
	}

	public GameCharacter getPusher(){
		return this.pusher;
	}

	/**
	 * 
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/**
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GameCharacter other = (GameCharacter) obj;
		if (id != other.id)
			return false;
		return true;
	}

	/**
	 * Compare To
	 */
	@Override
	public int compareTo(GameCharacter c) {
		if(this.y>c.y){
			return -1;
		}else if(this.y<c.y){
			return 1;
		}
		return 0;
	}

	public boolean isDead(){
		return dead;
	}

	/**
	 * Ajoute un item
	 * @param type
	 */
	public void addItem(int type){
		this.item = type;
		itemTime = 400;
		if(type == MapItem.ITEM_IRON_POTION){
			colorGray = 0.7f;
		}
	}

	protected boolean hasAWeapon(){
		return (item != -1);
	}

	public int getItemTime(){
		return itemTime;
	}

	public int getItem(){
		return item;
	}

	public boolean isActive(){
		return appearPlay && appear2Play && !fall && !dead && !fallOnBack;
	}

	public int getColdownPush() {
		return coldownPush;
	}

	public void setColdownPush(int coldownPush) {
		this.coldownPush = coldownPush;
	}
	
	public float getStrength(){
		return strength;
	}
	
	public int getPoint(){
		return 1;
	}



}
