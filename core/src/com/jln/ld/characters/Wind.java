package com.jln.ld.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.jln.ld.GameConstant;
import com.jln.ld.GameEntity;
import com.jln.ld.GameShared;
import com.jln.ld.GameUtils;
import com.jln.ld.menus.GameMenu;

public class Wind extends GameEntity{

	private float x;
	private float y;
	private boolean canBeDestroy = false;
	
	private Animation frames;
	private float stateTime = 0f;
	
	/**
	 * 
	 */
	public Wind(){
		this.x = (float) (640 + (Math.random()*1000));
		this.y = (float) (100 + (Math.random()*180));
		
		frames = GameUtils.loadAnimation(GameConstant.TX_WIND, 4, 0, 0.13f);
		frames.setPlayMode(PlayMode.LOOP);
		this.stateTime = 0f;
	}
	
	/**
	 * 
	 */
	public void update(GameMenu game){
		if(x<-64){
			this.canBeDestroy = true;
		}else{
			x-=3;
			stateTime += Gdx.graphics.getDeltaTime();  
		}
	}
	
	/**
	 * 
	 */
	public void render(GameMenu game){
		GameShared.instance.gameBatch.draw(frames.getKeyFrame(stateTime),x-16,y-16);
	}
	
	public boolean isCanBeDestroy(){
		return canBeDestroy;
	}
	
}
