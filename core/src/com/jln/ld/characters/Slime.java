package com.jln.ld.characters;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.jln.ld.GameConstant;
import com.jln.ld.GameEntity;
import com.jln.ld.GamePoint;
import com.jln.ld.GameShared;
import com.jln.ld.GameUtils;
import com.jln.ld.map.MapItem;
import com.jln.ld.menus.GameMenu;

public class Slime extends GameCharacter{

	public static final int CELL_WIDTH = 32;
	public static final int CELL_HEIGHT = 32;
	
	public static final int BASIC_SLIME = 0;
	public static final int SLOW_SLIME = 1;
	public static final int AGGRO_SLIME = 2;
	public static final int BIG_SLIME = 3;

	private Animation[] moveAnimation;

	private GamePoint target;

	private int timeBeforeAppear;


	private int slimeType;


	/**
	 * Constructeur du heros
	 */
	public Slime(float x, float y, int slimeType){
		super();
		this.x=x;
		this.y=y;
		this.slimeType = slimeType;
		target = new GamePoint(x,y);
		this.initRessources();
		this.collisionHeight = 16;
		this.collisionWidth = 16;
		
		this.initCollision();
		this.pushSound = GameConstant.SOUND_PUSH_SLIME;
		this.fallSound = GameConstant.SOUND_FALL;

		this.timeBeforeAppear = (int)(Math.random()*120)+60;

		if(this.slimeType == BASIC_SLIME){
			speed = 1.2f;
			strength = 4f;
		}else if(this.slimeType == SLOW_SLIME){
			speed = 0.7f;
			strength = 5f;
		}else if(this.slimeType == AGGRO_SLIME){
			speed = 0.8f;
			strength = 5f;
		}else if(this.slimeType == BIG_SLIME){
			speed = 1.1f;
			strength = 7f;
		}
	}

	/**
	 * Initialise les animations du perso
	 */
	protected void initRessources(){
		super.initRessources();
		this.moveAnimation = new Animation[5];
		TextureRegion[][] frames = null;
		if(slimeType == BASIC_SLIME){
			frames = TextureRegion.split(GameConstant.TX_SLIME_01, CELL_WIDTH, CELL_HEIGHT);
		}else if(slimeType == SLOW_SLIME){
			frames = TextureRegion.split(GameConstant.TX_SLIME_02, CELL_WIDTH, CELL_HEIGHT);
		}else if(slimeType == AGGRO_SLIME){
			frames = TextureRegion.split(GameConstant.TX_SLIME_03, CELL_WIDTH, CELL_HEIGHT);
		}else if(slimeType == BIG_SLIME){
			frames = TextureRegion.split(GameConstant.TX_SLIME_04, CELL_WIDTH, CELL_HEIGHT);
		}

		for(int i=0;i<moveAnimation.length;i++){
			moveAnimation[i] = GameUtils.loadAnimation(frames, 3, i, i==4?0.08f:0.15f);
			moveAnimation[i].setPlayMode(PlayMode.LOOP);
		}
	}

	/**
	 * Maj le heros
	 * @param game
	 */
	public void update(GameMenu game){
		if(timeBeforeAppear > 0){
			timeBeforeAppear--;
		}else{
			super.update(game);
			if(fall){
				//gerer dans le super
			}else if(pushX == 0 && pushY == 0 && appearPlay && appear2Play){
				this.updateMove(game);
			}
			stateTime += Gdx.graphics.getDeltaTime(); 
		}

	}

	/**
	 * Dessine le heros
	 * @param game
	 */
	public void render(GameMenu game){
		if(timeBeforeAppear<=0){
			super.render(game);
			if(appearPlay){
				colorBeforeDraw(game);
				GameShared.instance.gameBatch.draw(moveAnimation[direction].getKeyFrame(stateTimeMove), ((int)x)-(CELL_WIDTH/2), ((int)y)-(CELL_HEIGHT/2));
				colorBackToOrigine(game);
			}
		}
	}
	
	/**
	 * Ajoute une frame fantome
	 * @param game
	 * @param frames
	 */
	protected void addGhost(GameMenu game, List<GameCharacterGhostFrame> frames){
		frames.add(new GameCharacterGhostFrame(moveAnimation[direction].getKeyFrame(stateTimeMove), ((int)x)-(CELL_WIDTH/2), ((int)y)-(CELL_HEIGHT/2)));
		super.addGhost(game, frames);
	}

	/**
	 * Met a jour les deplacements du heros
	 * @param game
	 */
	private void updateMove(GameMenu game){
		float dx=0f;
		float dy=0f;

		if( (Math.abs(this.x-target.getX())<5 && Math.abs(this.y-target.getY())<5 ) || Math.random()<0.01){
			if(slimeType == AGGRO_SLIME){
				this.target = new GamePoint(game.getHeros().getX(), game.getHeros().getY());
			}else if(slimeType == BIG_SLIME){
				if(item == MapItem.NO_ITEM && !game.getMapItems().isEmpty()){
					this.target = new GamePoint(game.getMapItems().get(0).getX(),game.getMapItems().get(0).getY());
					this.changeDirection(game);
				}else{
					this.target = new GamePoint(game.getHeros().getX(), game.getHeros().getY());
					this.changeDirection(game);
				}
			}else if(slimeType == SLOW_SLIME){
				if(item == MapItem.NO_ITEM && !game.getMapItems().isEmpty()){
					this.target = new GamePoint(game.getMapItems().get(0).getX(),game.getMapItems().get(0).getY());
					this.changeDirection(game);
				}else{
					this.target = game.getMap().getRandomTargetPoint();
					this.changeDirection(game);
				}
			}else{
				this.target = game.getMap().getRandomTargetPoint();
				this.changeDirection(game);
			}
				
		}

		if(this.x > target.getX()){
			dx -= speed  * speedFactor;
		}else if(this.x < target.getX()){
			dx += speed  * speedFactor;
		}
		if(this.y > target.getY()){
			dy -= speed  * speedFactor;
		}else if(this.y < target.getY()){
			dy += speed  * speedFactor;
		}

		if(dx != 0f && dy != 0f){
			dx *= 0.75;
			dy *= 0.75;
		}

		
		if(dx !=0 || dy !=0){
			if(!this.isBlocked(game,x+dx, y+dy)){
				move(game,dx,dy);
			}else{
				this.target = game.getMap().getRandomTargetPoint();
			}

			stateTimeMove += Gdx.graphics.getDeltaTime(); 
		}
	}
	
	private void changeDirection(GameMenu game){
		float diffX = target.getX() - x;
		float diffY = target.getY() - y;

		if(Math.abs(diffX)>Math.abs(diffY)){
			if(diffX>0){
				direction = GameConstant.DIR_RIGHT;
			}else{
				direction = GameConstant.DIR_LEFT;
			}
		}else{
			if(diffY>0){
				direction = GameConstant.DIR_UP;
			}else{
				direction = GameConstant.DIR_DOWN;
			}
		}
	}
	
	/**
	 * Nombre de point quand mort
	 */
	public int getPoint(){
		return 2+slimeType;
	}
}


