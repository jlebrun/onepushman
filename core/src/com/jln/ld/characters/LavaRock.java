package com.jln.ld.characters;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.jln.ld.GameConstant;
import com.jln.ld.GameEntity;
import com.jln.ld.GamePoint;
import com.jln.ld.GameShared;
import com.jln.ld.GameUtils;
import com.jln.ld.map.MapItem;
import com.jln.ld.menus.GameMenu;

public class LavaRock extends GameCharacter{

	public static final int CELL_WIDTH = 32;
	public static final int CELL_HEIGHT = 32;
	
	public static final int BASIC_LAVAROCK = 0;
	public static final int BIG_LAVAROCK = 1;
	public static final int AGGRO_SLIME = 0;

	private Animation[] moveAnimation;

	private float speed = 1.2f;

	private GamePoint target;

	private int timeBeforeAppear;
	private int rockType;

	private boolean angry = false;
	private int cptAngry = 0;

	/**
	 * Constructeur du heros
	 */
	public LavaRock(float x, float y, int rockType){
		super();
		this.x=x;
		this.y=y;
		this.rockType = rockType;
		target = new GamePoint(x,y);
		this.initRessources();
		this.collisionHeight = 16;
		this.collisionWidth = 16;
		
		
		this.initCollision();
		this.pushSound = GameConstant.SOUND_PUSH_SLIME;
		this.fallSound = GameConstant.SOUND_FALL;

		this.timeBeforeAppear = (int)(Math.random()*120)+60;

		this.initSpeed();
	}
	
	private void initSpeed(){
		if(this.rockType == BASIC_LAVAROCK){
			speed = 1f;
			strength = 5f;
		}else if(this.rockType == BIG_LAVAROCK){
			speed = 0.6f;
			strength = 8f;
		}
		if(angry){
			speed = speed*1.5f;
		}
	}

	/**
	 * Initialise les animations du perso
	 */
	protected void initRessources(){
		super.initRessources();
		this.moveAnimation = new Animation[10];
		TextureRegion[][] frames = null;
		if(rockType == BASIC_LAVAROCK){
			frames = TextureRegion.split(GameConstant.TX_LAVAROCK_01, CELL_WIDTH, CELL_HEIGHT);
		}else if(rockType == BIG_LAVAROCK){
			frames = TextureRegion.split(GameConstant.TX_LAVAROCK_02, CELL_WIDTH, CELL_HEIGHT);
		}

		for(int i=0;i<moveAnimation.length;i++){
			moveAnimation[i] = GameUtils.loadAnimation(frames, 3, i, i==4?0.08f:0.15f);
			moveAnimation[i].setPlayMode(PlayMode.LOOP);
		}
	}

	/**
	 * Maj l'ennemi
	 * @param game
	 */
	public void update(GameMenu game){
		if(timeBeforeAppear > 0){
			timeBeforeAppear--;
		}else{
			super.update(game);
			if(cptAngry>0){
				cptAngry--;
				if(cptAngry==0){
					angry = false;
					this.initSpeed();
				}
			}
			if(fall){
				//gerer dans le super
			}else if(pushX == 0 && pushY == 0 && appearPlay && appear2Play){
				this.updateMove(game);
			}
			stateTime += Gdx.graphics.getDeltaTime(); 
		}

	}

	/**
	 * Dessine le heros
	 * @param game
	 */
	public void render(GameMenu game){
		if(timeBeforeAppear<=0){
			super.render(game);
			if(appearPlay){
				colorBeforeDraw(game);
				GameShared.instance.gameBatch.draw(moveAnimation[angry?direction+5:direction].getKeyFrame(stateTimeMove), ((int)x)-(CELL_WIDTH/2), ((int)y)-(CELL_HEIGHT/2));
				colorBackToOrigine(game);
			}
		}
	}
	
	/**
	 * Ajoute une frame fantome
	 * @param game
	 * @param frames
	 */
	protected void addGhost(GameMenu game, List<GameCharacterGhostFrame> frames){
		frames.add(new GameCharacterGhostFrame(moveAnimation[angry?direction+5:direction].getKeyFrame(stateTimeMove), ((int)x)-(CELL_WIDTH/2), ((int)y)-(CELL_HEIGHT/2)));
		super.addGhost(game, frames);
	}
	
	

	/**
	 * Met a jour les deplacements du heros
	 * @param game
	 */
	private void updateMove(GameMenu game){
		float dx=0f;
		float dy=0f;

		if( (Math.abs(this.x-target.getX())<5 && Math.abs(this.y-target.getY())<5 ) || Math.random()<0.01){
			if(angry){
				this.target = new GamePoint(game.getHeros().getX(), game.getHeros().getY());
			}else{
				if(item == MapItem.NO_ITEM && !game.getMapItems().isEmpty()){
					this.target = new GamePoint(game.getMapItems().get(0).getX(),game.getMapItems().get(0).getY());
				}else{
					if(game.getHeros().getItem() == MapItem.NO_ITEM){
						this.target = new GamePoint(game.getHeros().getX(), game.getHeros().getY());
					}else{
						this.target = game.getMap().getRandomTargetPoint();
					}
				}	
			}
			
		}

		if(this.x > target.getX()){
			dx -= speed  * speedFactor;
		}else if(this.x < target.getX()){
			dx += speed  * speedFactor;
		}
		if(this.y > target.getY()){
			dy -= speed  * speedFactor;
		}else if(this.y < target.getY()){
			dy += speed  * speedFactor;
		}

		if(dx != 0f && dy != 0f){
			dx *= 0.75;
			dy *= 0.75;
		}

		if(dx>0){
			direction = GameConstant.DIR_RIGHT;
		}else if(dx<0){
			direction = GameConstant.DIR_LEFT;
		}

		if(dy>0){
			direction = GameConstant.DIR_UP;
		}else if(dy<0){
			direction = GameConstant.DIR_DOWN;

		}
		if(dx !=0 || dy !=0){
			if(!this.isBlocked(game,x+dx, y+dy)){
				move(game,dx,dy);
			}else{
				this.target = game.getMap().getRandomTargetPoint();
			}

			stateTimeMove += Gdx.graphics.getDeltaTime(); 
		}
	}
	
	public boolean isAngry(){
		return angry;
	}
	
	public void angry(GameMenu game){
		this.cptAngry = 300;
		this.angry = true;
		this.initSpeed();
		this.target = new GamePoint(game.getHeros().getX(), game.getHeros().getY());
	}
	
	/**
	 * Redefinition de la m�thde "est pouss�"
	 */
	public void setPusher(GameMenu game,GameCharacter pusher) {
		super.setPusher(game,pusher);
		if(rockType==BIG_LAVAROCK && pusher.equals(game.getHeros())){
			for(GameCharacter c : game.getCharacters()){
				if(c instanceof LavaRock){
					LavaRock lr = (LavaRock)c;
					lr.angry(game);
				}
			}
		}
	}

	/**
	 * Nombre de point quand mort
	 */
	public int getPoint(){
		if(this.rockType == BASIC_LAVAROCK){
			return 2;
		}else if(this.rockType == BIG_LAVAROCK){
			return 5;
		}
		return 1;
	}
}
