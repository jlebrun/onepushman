package com.jln.ld.characters;


import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class GameCharacterGhostFrame {

	private TextureRegion frame;
	private float x;
	private float y;
	
	public GameCharacterGhostFrame(TextureRegion frame,float x, float y){
		this.x = x;
		this.y = y;
		this.frame = frame;
	}

	public TextureRegion getFrame() {
		return frame;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	
	
	
}
