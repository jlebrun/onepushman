package com.jln.ld.characters;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.jln.ld.GameConstant;
import com.jln.ld.GameEntity;
import com.jln.ld.GameShared;
import com.jln.ld.GameUtils;
import com.jln.ld.map.MapItem;
import com.jln.ld.menus.GameMenu;
import com.jln.ld.menus.PlayerSelectionItem;

public class Heros extends GameCharacter{

	public static final int CELL_WIDTH = 32;
	public static final int CELL_HEIGHT = 32;

	private Animation[] moveAnimation;

	private float speed = 1f;
	private float accelerationX = 0f;
	private float accelerationY = 0f;
	private int cptFreeze = 0;
	protected float dx, dy;

	protected boolean onMove = false;
	protected Rectangle coreContactRectangle;

	protected String message = "";
	protected int cptMessage;


	/**
	 * Constructeur du heros
	 */
	public Heros(){
		super();
		x=320;
		y=170;
		this.collisionHeight = 20;
		this.collisionWidth = 20;

		this.initCollision();
		this.initRessources();
		this.pushSound = GameConstant.SOUND_PUSH_HEROS;
		this.fallSound = GameConstant.SOUND_FALL;
		this.decalRenderY = 8; 
		
		speed = GameShared.instance.player1Character.getFloatSpeed();
		strength = GameShared.instance.player1Character.getFloatStrength();
		
		System.out.println("SPEED = "+speed+" STR ="+strength);
	}

	/**
	 * Initialise les animations du perso
	 */
	@Override
	public void initRessources(){
		super.initRessources();
		this.moveAnimation = new Animation[5];

		TextureRegion[][] frames = null;
		if(GameShared.instance.player1Character.getCharacter() == PlayerSelectionItem.CHARACTER.GREENBOY){
			frames = TextureRegion.split(GameConstant.TX_GREENBOY, CELL_WIDTH, CELL_HEIGHT);
		}else if(GameShared.instance.player1Character.getCharacter() == PlayerSelectionItem.CHARACTER.BLACKSOLDIER){
			frames = TextureRegion.split(GameConstant.TX_BLACKSOLDIER, CELL_WIDTH, CELL_HEIGHT);
		}else if(GameShared.instance.player1Character.getCharacter() == PlayerSelectionItem.CHARACTER.WIZARD){
			frames = TextureRegion.split(GameConstant.TX_WIZARD, CELL_WIDTH, CELL_HEIGHT);
		}else if(GameShared.instance.player1Character.getCharacter() == PlayerSelectionItem.CHARACTER.HARUSAME){
			frames = TextureRegion.split(GameConstant.TX_HARUSAME, CELL_WIDTH, CELL_HEIGHT);
		}else if(GameShared.instance.player1Character.getCharacter() == PlayerSelectionItem.CHARACTER.CAULFIELD){
			frames = TextureRegion.split(GameConstant.TX_CAULFIELD, CELL_WIDTH, CELL_HEIGHT);
		}else if(GameShared.instance.player1Character.getCharacter() == PlayerSelectionItem.CHARACTER.FLANE){
			frames = TextureRegion.split(GameConstant.TX_FLANE, CELL_WIDTH, CELL_HEIGHT);
		}else if(GameShared.instance.player1Character.getCharacter() == PlayerSelectionItem.CHARACTER.RELM){
			frames = TextureRegion.split(GameConstant.TX_RELM, CELL_WIDTH, CELL_HEIGHT);
		}else if(GameShared.instance.player1Character.getCharacter() == PlayerSelectionItem.CHARACTER.GARYWISS){
			frames = TextureRegion.split(GameConstant.TX_GARYWISS, CELL_WIDTH, CELL_HEIGHT);
		}

		for(int i=0;i<moveAnimation.length;i++){
			moveAnimation[i] = GameUtils.loadAnimation(frames, 3, i, i==4?0.08f:0.15f);
			moveAnimation[i].setPlayMode(PlayMode.LOOP);
		}
	}

	/**
	 * Maj le heros
	 * @param game
	 */
	public void update(GameMenu game){
		super.update(game);
		if(cptFreeze > 0){
			cptFreeze --;
		}
		if(fall){
			// gerer dans le super
		}else if(pushX == 0 && pushY == 0 && cptFreeze<=0 && appearPlay && appear2Play){
			this.updateMove(game);
		}

		if(cptMessage > 0){
			cptMessage --;
			if(cptMessage <=0){
				this.message = null;
			}

		}
		stateTime += Gdx.graphics.getDeltaTime(); 
	}

	/**
	 * 
	 * @param game
	 */
	protected void die(GameMenu game){
		super.die(game);
		game.getHud().updateLife(game, true, -1);
		if(game.getHud().getLife()>0){
			this.appear2Play = false;
			this.appearPlay = false;
			this.x = (GameConstant.GAME_WIDTH/2);
			this.y = (GameConstant.GAME_HEIGHT/2);
			this.fall = false;
			this.fallAcceleration = 0;
			this.fallOnBack = false;
			this.dead = false;
			this.stateTime = 0f;
			this.initCollision();
			if(Math.random()<0.25){
				addMessage("Oops, I slipped...");
			}
		}else{
			game.getHud().gameOver(game);
		}
	}

	/**
	 * Dessine le heros
	 * @param game
	 */
	public void render(GameMenu game){
		super.render(game);
		if(appearPlay){
			colorBeforeDraw(game);
			GameShared.instance.gameBatch.draw(onMove?moveAnimation[direction].getKeyFrame(stateTimeMove):moveAnimation[direction].getKeyFrames()[1], ((int)x)-(CELL_WIDTH/2), ((int)y)-(CELL_HEIGHT/2)+decalRenderY);
			colorBackToOrigine(game);
		}
	}
	
	/**
	 * Ajoute une frame fantome
	 * @param game
	 * @param frames
	 */
	protected void addGhost(GameMenu game, List<GameCharacterGhostFrame> frames){
		frames.add(new GameCharacterGhostFrame(onMove?moveAnimation[direction].getKeyFrame(stateTimeMove):moveAnimation[direction].getKeyFrames()[1], ((int)x)-(CELL_WIDTH/2), ((int)y)-(CELL_HEIGHT/2)+decalRenderY));
		super.addGhost(game, frames);
	}

	/**
	 * Dessine les messages du heros
	 * @param game
	 */
	public void renderMessage(GameMenu game){
		if(message != null && !message.isEmpty()){
			if(cptMessage<100){
				GameShared.instance.font.setColor(0.9f, 0.9f, 0.9f, ((float)(cptMessage)/100f));
			}
			GameShared.instance.font.draw(GameShared.instance.gameBatch,message,x-(message.length()*5),y+48);
			if(cptMessage<100){
				GameShared.instance.font.setColor(1, 1, 1, 1);
			}
		}
	}

	/**
	 * Met a jour les deplacements du heros
	 * @param game
	 */
	private void updateMove(GameMenu game){
		dx=0f;
		dy=0f;

		if(Gdx.input.isKeyPressed(Keys.LEFT)){
			dx = -speed * speedFactor;
			direction = GameConstant.DIR_LEFT;
		}
		if(Gdx.input.isKeyPressed(Keys.RIGHT)){
			dx = speed  * speedFactor;
			direction = GameConstant.DIR_RIGHT;
		}
		if(Gdx.input.isKeyPressed(Keys.DOWN)){
			dy = -speed  * speedFactor;
			direction = GameConstant.DIR_DOWN;
		}
		if(Gdx.input.isKeyPressed(Keys.UP)){
			dy = speed  * speedFactor;
			direction = GameConstant.DIR_UP;
		}

		if(dx != 0f && dy != 0f){
			dx *= 0.75;
			dy *= 0.75;
		}

		this.updateAcceleration();

		if(dx !=0 || dy !=0){
			this.onMove = true;
			if(!this.isBlocked(game,x+dx, y+dy)){
				move(game,dx,dy);
				if(dx>0 && accelerationX<speed){
					this.accelerationX+=0.05f;
				}else if(dx<0 && accelerationX>-speed){
					this.accelerationX-=0.05f;
				}
				if(dy>0 && accelerationY<speed){
					this.accelerationY+=0.05f;
				}else if(dy<0 && accelerationY>-speed){
					this.accelerationY-=0.05f;
				}
				
			}else if(dx != 0 && dy != 0){
				if(!this.isBlocked(game,x, y+dy)){
					move(game,0,dy);
					if(dy>0 && accelerationY<speed){
						this.accelerationY+=0.05f;
					}else if(dy<0 && accelerationY>-speed){
						this.accelerationY-=0.05f;
					}
				}else if(!this.isBlocked(game,x+dx, y) ){
					move(game,dx,0);
					if(dx>0 && accelerationX<speed){
						this.accelerationX+=0.05f;
					}else if(dx<0 && accelerationX>-2){
						this.accelerationX-=0.05f;
					}
				}else if(this.isBlocked(game,x, y)){
					move(game,0,-1);
				}
			}

			stateTimeMove += Gdx.graphics.getDeltaTime(); 
		}else{
			onMove = false;
		}
	}

	private void updateAcceleration(){
		if(dx == 0 && this.accelerationX != 0){
			if(this.accelerationX>0){
				this.accelerationX-=0.1;
				if(this.accelerationX<=0){
					this.accelerationX = 0;
				}
			}else if(this.accelerationX<0){
				this.accelerationX+=0.1;
				if(this.accelerationX>=0){
					this.accelerationX = 0;
				}
			}
		}

		if(dx > 0 && this.accelerationX < 0){
			this.accelerationX+=0.15;
			if(this.accelerationX>=0){
				this.accelerationX = 0;
			}
		}
		if(dx < 0 && this.accelerationX > 0){
			this.accelerationX-=0.15;
			if(this.accelerationX<=0){
				this.accelerationX = 0;
			}
		}
		if(dy == 0){
			if(this.accelerationY>0){
				this.accelerationY-=0.1;
				if(this.accelerationY<=0){
					this.accelerationY = 0;
				}
			}else if(this.accelerationY<0){
				this.accelerationY+=0.1;
				if(this.accelerationY>=0){
					this.accelerationY = 0;
				}
			}
		}

		if(dy > 0 && this.accelerationY < 0){
			this.accelerationY+=0.15;
			if(this.accelerationY>=0){
				this.accelerationY = 0;
			}
		}
		if(dy < 0 && this.accelerationY > 0){
			this.accelerationY-=0.15;
			if(this.accelerationY<=0){
				this.accelerationY = 0;
			}
		}

		dx += accelerationX;
		dy += accelerationY;
	}

	public void setPusher(GameMenu game,GameCharacter pusher) {
		super.setPusher(game,pusher);
		this.accelerationX = 0f;
		this.accelerationY= 0f;
		this.cptFreeze = 10;
	}
	
	

	@Override
	public void addItem(int type){
		super.addItem(type);
		if(type == MapItem.WEAPON_SHOVEL){
			double d = Math.random();
			if(d < 0.25){
				addMessage("I'm the shovel knight !");
			}else if(d < 0.5){
				addMessage("I will dig in their heads.");
			}
		}else if(type == MapItem.ITEM_IRON_POTION){
			double d = Math.random();
			if(d < 0.25){
				addMessage("I'm super heavy ! But so slow...");
			}else if(d < 0.5){
				addMessage("I have eaten too much.");
			}
		}else if(type == MapItem.WEAPON_HAMMER){
			double d = Math.random();
			if(d < 0.25){
				addMessage("Oh yeah, a big HAMMER !");
			}
		}else if(type == MapItem.WEAPON_HAMMER){
			double d = Math.random();
			if(d < 0.25){
				addMessage("I'm Flash !");
			}
		}
	}

	/**
	 * 
	 * @param s
	 */
	public void addMessage(String s){
		if(cptMessage<=0 || message == null){
			this.message = s;
			this.cptMessage = 240;
		}
	}

}
