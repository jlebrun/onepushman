package com.jln.ld.characters;

import java.util.List;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jln.ld.GameEntity;
import com.jln.ld.GameShared;
import com.jln.ld.menus.GameMenu;

public class GameCharacterGhost extends GameEntity{

	private List<GameCharacterGhostFrame> frames;
	private float x;
	private float y;
	private float opacity;
	
	public GameCharacterGhost(float x, float y, List<GameCharacterGhostFrame> frames){
		this.x = x;
		this.y = y;
		this.frames = frames;
		this.opacity = 0.7f;
	}
	
	public void render(GameMenu game){
		GameShared.instance.gameBatch.setColor(1f, 1f, 1f, opacity);
		for (GameCharacterGhostFrame g : frames) {
			GameShared.instance.gameBatch.draw(g.getFrame(), g.getX(), g.getY());
		}
		GameShared.instance.gameBatch.setColor(1f, 1f, 1f, 1f);
	}
	
	public void update(GameMenu game){
		this.opacity -= 0.03f;
		if(this.opacity<0){
			this.opacity = 0;
		}
	}
	
	public boolean isDestroy(){
		return this.opacity<=0;
	}
}
