package com.jln.ld.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.jln.ld.Ld38Launcher;
import com.jln.ld.GameConstant;


public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = GameConstant.GAME_WIDTH;
		config.height = GameConstant.GAME_HEIGHT;
		config.backgroundFPS = 60;
		config.foregroundFPS = 60;
		config.title = GameConstant.GAME_TITLE ;
		new LwjglApplication(new Ld38Launcher(), config);
	}
}
